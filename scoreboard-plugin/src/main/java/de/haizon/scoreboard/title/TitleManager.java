/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.haizon.scoreboard.title;

/*

  » de.haizon.scoreboard.title

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 11:56

 */

import de.thundercloud.module.player.IPermissionPlayer;
import de.thundercloud.module.pool.PermissionPool;
import de.thundercloud.plugin.CloudAPI;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TitleManager {

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

    public void sendActionbar(){
        CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayers().forEach(iCloudPlayer -> {

            IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPlayer(iCloudPlayer.getUniqueId());

            Date date = new Date();

            iCloudPlayer.sendActionBar("§8» §fLobby §8× §7Rang §8➟ §f" + permissionPlayer.getHighestGroup().getName() + " §7Uhrzeit §8➟ §f" + simpleDateFormat.format(date));

        });
    }

}
