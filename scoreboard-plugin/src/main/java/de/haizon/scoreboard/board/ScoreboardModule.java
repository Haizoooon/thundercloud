/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.haizon.scoreboard.board;

/*

  » de.haizon.scoreboard.board

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 09:03

 */

import de.thundercloud.module.player.IPermissionPlayer;
import de.thundercloud.module.pool.PermissionPool;
import org.bukkit.entity.Player;

public class ScoreboardModule {

    public void setLobby(Player player){

        ScoreboardAPI scoreboardAPI = new ScoreboardAPI(player, "§8» §f§lLobby");

        IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPlayer(player.getUniqueId());

        scoreboardAPI.setLine(14, "§8§m-------", "§8§m-------");
        scoreboardAPI.setLine(13, " ", " ");
        scoreboardAPI.setLine(12, " §8» ", "§7Coins");
        scoreboardAPI.setLine(11, " §8➜ ", "§f" + "16912");
        scoreboardAPI.setLine(10, " ", " ");
        scoreboardAPI.setLine(9, " §8» ", "§7Rang");
        scoreboardAPI.setLine(8, " §8➜ ", "§f" + permissionPlayer.getHighestGroup().getName());
        scoreboardAPI.setLine(7, " ", " ");
        scoreboardAPI.setLine(6, " §8» ", "§7Freunde");
        scoreboardAPI.setLine(5, " §8➜ ", "§4§l✖");
        scoreboardAPI.setLine(4, " ", " ");
        scoreboardAPI.setLine(3, " §8» ", "§7Server");
        scoreboardAPI.setLine(2, " §8➜ ", "§f" + "Lobby-1");
        scoreboardAPI.setLine(1, " ", " ");
        scoreboardAPI.setLine(0, "§8§m-------", "§8§m-------");

        scoreboardAPI.setBoard(player);

    }

}
