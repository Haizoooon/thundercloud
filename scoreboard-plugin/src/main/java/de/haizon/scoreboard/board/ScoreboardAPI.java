/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.haizon.scoreboard.board;

/*

  » de.haizon.scoreboard.board

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 09:03

 */

import com.google.common.collect.Maps;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.Map;

public class ScoreboardAPI {

    public final Map<Integer, String> scoreMap = Maps.newConcurrentMap();
    public final Map<String, Integer> scheduleMap = Maps.newConcurrentMap();
    public String displayName;
    public String[] animation;
    public Player player;

    public ScoreboardAPI(Player player, String displayName) {
        this.player = player;
        this.displayName = displayName;
    }

    public String getLine(int line) {
        return this.scoreMap.get(line);
    }

    public void setLine(int score, String prefix, String suffix) {
        this.scoreMap.put(score, prefix + ";" + suffix);
    }

    public void setBoard(Player player) {
        player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("aaa", "bbb");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName(this.displayName);
        for (int i = 0; i < 20; i++) {
            if (this.scoreMap.get(i) != null) {
                Team team = scoreboard.registerNewTeam("x" + i);
                String[] raw = (this.scoreMap.get(i)).split(";");
                team.setPrefix(raw[0]);
                team.setSuffix(raw[1]);
                if (i < 10) {
                    team.addEntry("§" + i);
                    objective.getScore("§" + i).setScore(i);
                } else {
                    team.addEntry("§" + getColorCode(i));
                    objective.getScore("§" + getColorCode(i)).setScore(i);
                }
                player.setScoreboard(scoreboard);
            }
        }
    }

    public void updateBoard(Player player, int score, String prefix, String suffix) {
        if (player.getScoreboard() != null && player.getScoreboard().getObjective(DisplaySlot.SIDEBAR) != null) {
            player.getScoreboard().getTeam("x" + score).setSuffix(suffix);
            player.getScoreboard().getTeam("x" + score).setPrefix(prefix);
        }
    }

    public void addAnimation(String[] animationCont, Plugin plugin) {
        this.animation = animationCont;
        Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
            int animationTick = 0;

            public void run() {
                if (this.animationTick == ScoreboardAPI.this.animation.length)
                    this.animationTick = 0;
                if (ScoreboardAPI.this.player.getScoreboard() != null)
                    try {
                        ScoreboardAPI.this.player.getScoreboard().getObjective(DisplaySlot.SIDEBAR).setDisplayName(ScoreboardAPI.this.animation[this.animationTick]);
                    } catch (Exception ignored) {}
                this.animationTick++;
            }
        },  0L, 10L);
    }

    public void stopAnimation() {
        Bukkit.getScheduler().cancelTask(this.scheduleMap.get("scheduler"));
    }

    public String getColorCode(int number) {
        switch (number) {
            case 10:
                return "a";
            case 11:
                return "b";
            case 12:
                return "c";
            case 13:
                return "d";
            case 14:
                return "e";
            case 15:
                return "f";
        }
        return "z";
    }

    public Player getPlayer() {
        return this.player;
    }

}
