/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.haizon.scoreboard;

/*

  » de.haizon.scoreboard

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 09:02

 */

import de.haizon.scoreboard.events.PlayerJoinListener;
import de.haizon.scoreboard.title.TitleManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ScoreboardBootstrap extends JavaPlugin {

    @Override
    public void onEnable() {

        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, () -> new TitleManager().sendActionbar(), 0, 20);

    }
}
