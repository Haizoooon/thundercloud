/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.proxy;

/*

  » de.thundercloud.proxy

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 20:11

 */

import de.thundercloud.api.module.Module;
import de.thundercloud.launcher.console.logging.LoggerProvider;

@Module(name = "proxy-module", reloadable = true, authors = {"Haizoooon", "VorGecodet"})
public class ProxyModule {

    private LoggerProvider loggerProvider;

    public void onInitialization(LoggerProvider loggerProvider){
        this.loggerProvider = loggerProvider;



    }

    public LoggerProvider getLoggerProvider() {
        return loggerProvider;
    }
}
