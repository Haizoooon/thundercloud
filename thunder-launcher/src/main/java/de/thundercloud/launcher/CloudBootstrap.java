package de.thundercloud.launcher;

/*

  » de.thundercloud.launcher

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 30.03.2021 / 20:41

 */

import de.thundercloud.launcher.console.ConsoleManager;
import de.thundercloud.launcher.console.ConsoleSender;
import de.thundercloud.launcher.console.command.CommandManager;
import de.thundercloud.launcher.console.logging.LoggerProvider;
import de.thundercloud.launcher.files.FileHandler;

public class CloudBootstrap {

    private static CloudBootstrap bootstrap;
    private final ConsoleManager consoleManager;
    private final LoggerProvider loggerProvider;
    private final CommandManager commandManager;
    private final ConsoleSender consoleSender;
    private final FileHandler fileHandler;

    private boolean setup = false;

    public static void main(String[] args) {
        new CloudBootstrap();
    }

    public CloudBootstrap(){
        bootstrap = this;

        this.fileHandler = new FileHandler();
        this.consoleManager = new ConsoleManager();
        this.loggerProvider = new LoggerProvider();
        this.commandManager = new CommandManager();
        this.consoleSender = new ConsoleSender();
        this.commandManager.register("de.thundercloud.launcher.commands");

    }

    public FileHandler getFileHandler() {
        return fileHandler;
    }

    public boolean isSetup() {
        return setup;
    }

    public void setSetup(boolean setup) {
        this.setup = setup;
    }

    public ConsoleSender getConsoleSender() {
        return consoleSender;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public LoggerProvider getLoggerProvider() {
        return loggerProvider;
    }

    public ConsoleManager getConsoleManager() {
        return consoleManager;
    }

    public static CloudBootstrap getBootstrap() {
        return bootstrap;
    }
}
