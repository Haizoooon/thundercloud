/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.launcher.async;

/*

  » de.thundercloud.launcher.async

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 12:14

 */

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Supplier;

public abstract class AsyncTask {

    public static <U> void supplyAsync(Supplier<U> supplier){

        ExecutorService executorService = Executors.newCachedThreadPool();
        Future<U> futureTask = executorService.submit(supplier::get);
        if(!futureTask.isDone()){
            return;
        }
        executorService.shutdown();

    }

}
