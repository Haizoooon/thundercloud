/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.launcher.external.event;

/*

  » de.thundercloud.launcher.external.event

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 20:29

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.event.IEvent;
import de.thundercloud.api.event.IEventManager;
import de.thundercloud.launcher.CloudBootstrap;

import java.util.List;

public class EventManager implements IEventManager {

    private final List<IEvent> events;

    public EventManager() {
        this.events = Lists.newArrayList();
    }

    @Override
    public void registerEvent(IEvent event) {
        events.add(event);
        CloudBootstrap.getBootstrap().getLoggerProvider().debug(event.getClass().getCanonicalName() + " was registered");
    }

    @Override
    public void callEvent(IEvent event) {
        //TODO: call a event
        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<IEvent> getEvents() {
        return events;
    }
}
