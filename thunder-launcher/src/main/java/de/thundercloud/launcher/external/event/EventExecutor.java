/*
 * » Created by Niklas Sch. on 8.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.launcher.external.event;

/*

  » de.thundercloud.launcher.external.event

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 08.04.2021 / 10:44

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.event.EventHandler;
import de.thundercloud.api.event.IEvent;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

public class EventExecutor {

    public List<Method> getMethods(IEvent event) {

        List<Method> methods = Lists.newArrayList();
        Class<?> clazz = event.getClass();
        while(clazz != Object.class){
            Arrays.stream(clazz.getDeclaredMethods()).filter(method -> method.isAnnotationPresent(EventHandler.class)).forEach(methods::add);
            clazz = clazz.getSuperclass();
        }

        return methods;

    }

}
