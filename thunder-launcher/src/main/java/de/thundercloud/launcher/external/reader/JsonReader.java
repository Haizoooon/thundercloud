/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.launcher.external.reader;

/*

  » de.thundercloud.launcher.external.reader

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:37

 */

import org.json.JSONObject;

public class JsonReader {

    private final JSONObject jsonObject;

    public JsonReader(String content) {
        this.jsonObject = new JSONObject(content);
    }

    public Object read(String var1){
        return jsonObject.get(var1);
    }
}
