package de.thundercloud.launcher.setups;

/*

  » de.thundercloud.launcher.setups

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 02.04.2021 / 09:51

 */

import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.launcher.console.setup.SetupBuilder;
import de.thundercloud.launcher.console.setup.abstracts.SetupEnd;
import de.thundercloud.launcher.console.setup.abstracts.SetupInput;
import de.thundercloud.launcher.console.setup.interfaces.ISetup;

import java.util.Arrays;
import java.util.List;

public class TestSetup implements ISetup {

    private String name;

    public TestSetup() {
        new SetupBuilder(this, new SetupEnd() {
            @Override
            public void handle() {
                CloudBootstrap.getBootstrap().getLoggerProvider().info(name);
            }
        }, new SetupInput("Hello") {
            @Override
            public List<String> getSuggestions() {
                return Arrays.asList("LOL", "LOL2");
            }

            @Override
            public boolean handle(String input) {
                name = input;
                return true;
            }
        });
    }
}
