package de.thundercloud.launcher.commands;

/*

  » de.thundercloud.launcher.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 21:10

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.launcher.setups.TestSetup;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "test", type = CommandType.CONSOLE)
public class TestCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender commandSender, String[] args) {
        if (args[0].equalsIgnoreCase("test")) {
            if(args.length == 2){
                new TestSetup();
            }
        }
    }

    @Override
    public String[] getUsage() {
        return new String[0];
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

}
