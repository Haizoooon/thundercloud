package de.thundercloud.launcher.console.logging;

/*

  » de.thundercloud.launcher.console.logging

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 30.03.2021 / 20:41

 */

import de.thundercloud.api.console.Color;
import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.api.console.LogType;
import org.jline.reader.LineReader;
import org.jline.utils.InfoCmp;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerProvider extends Logger {

    private final CopyOnWriteArrayList<String> cachedMessages = new CopyOnWriteArrayList<>();

    private final File logDir = new File("logs/");

    public LoggerProvider() {
        super("ThunderCloudLogger", null);

        setLevel(Level.ALL);
        setUseParentHandlers(false);

        logDir.mkdirs();

        System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tT] [%4$-7s] %5$s %n");

        File file = new File("logs/");
        SimpleFormatter simpleFormatter = new SimpleFormatter();

        try {

            FileHandler fileHandler = new FileHandler(file.getCanonicalPath() + "/thundercloud-log", 5242880, 100, false);
            fileHandler.setEncoding(StandardCharsets.UTF_8.name());
            fileHandler.setLevel(Level.ALL);
            fileHandler.setFormatter(simpleFormatter);

            addHandler(fileHandler);

        } catch (IOException e) {
            e.printStackTrace();
        }

        deleteOldLogs();

        write("\n___________.__                      .___            _________ .__                   .___\n" +
                "\\__    ___/|  |__  __ __  ____    __| _/___________ \\_   ___ \\|  |   ____  __ __  __| _/\n" +
                "  |    |   |  |  \\|  |  \\/    \\  / __ |/ __ \\_  __ \\/    \\  \\/|  |  /  _ \\|  |  \\/ __ | \n" +
                "  |    |   |   Y  \\  |  /   |  \\/ /_/ \\  ___/|  | \\/\\     \\___|  |_(  <_> )  |  / /_/ | \n" +
                "  |____|   |___|  /____/|___|  /\\____ |\\___  >__|    \\______  /____/\\____/|____/\\____ | \n" +
                "                \\/           \\/      \\/    \\/               \\/                       \\/ \n\n");
        write(LogType.INFO, "Cloud was developed by " + Color.CYAN.getColor() + "Haizoooon, YyTFlo and HttxDeVii");
    }

    public void write(String message){
        LineReader lineReader = CloudBootstrap.getBootstrap().getConsoleManager().getLineReader();
        lineReader.getTerminal().puts(InfoCmp.Capability.carriage_return);
        lineReader.getTerminal().writer().println(message);
        lineReader.getTerminal().flush();
        cachedMessages.add(message);
    }

    private void write(LogType logType, String message){
        Date date = new Date();
        String format = Color.toColoredString("§r» [" + new SimpleDateFormat("HH:mm:ss").format(date) + "] " + logType.getColor().getColor() + logType.getDisplay() + Color.RESET.getColor() + ": " + message);

        LineReader lineReader = CloudBootstrap.getBootstrap().getConsoleManager().getLineReader();
        lineReader.getTerminal().puts(InfoCmp.Capability.carriage_return);
        lineReader.getTerminal().writer().println(format);
        lineReader.getTerminal().flush();

        if(lineReader.isReading()){
            lineReader.callWidget(LineReader.REDRAW_LINE);
            lineReader.callWidget(LineReader.REDISPLAY);
        }

        cachedMessages.add(format);
    }

    @Override
    public void info(String msg) {
        super.info(msg);
        write(LogType.INFO, msg);
    }

    @Override
    public void warning(String msg) {
        super.warning(msg);
        write(LogType.WARNING, msg);
    }

    @Override
    public void severe(String msg) {
        super.severe(msg);
        write(LogType.ERROR, msg);
    }

    public void setup(String msg){
        super.info(msg);
        write(LogType.SETUP, msg);
    }

    public void debug(String msg){
        super.info(msg);
        write(LogType.DEBUG, msg);
    }

    public void deleteOldLogs(){
        File[] files = logDir.listFiles();
        if(files == null){
            return;
        }
        Arrays.stream(files).forEach(file -> {
            if (isOlder(file)) {
                file.delete();
            }
        });
    }

    private boolean isOlder(File file){
        long millis = TimeUnit.DAYS.toMillis(10);
        return (System.currentTimeMillis() - file.lastModified()) > millis;
    }

    public void clear(){
        System.out.print("\u001b[H\u001b[2J");
        System.out.flush();
    }

    public CopyOnWriteArrayList<String> getCachedMessages() {
        return cachedMessages;
    }

}
