package de.thundercloud.launcher.console;

/*

  » de.thundercloud.launcher.console

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 30.03.2021 / 20:41

 */

import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.Color;
import com.google.common.base.Charsets;
import de.thundercloud.launcher.CloudBootstrap;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.io.IOException;

public class ConsoleManager {

    private final LineReader lineReader;
    private Thread thread;
    private final ConsoleCompleter consoleCompleter;

    private final String prompt = Color.RESET.getColor() + "» " + Color.CYAN.getColor() + "ThunderCloud " + Color.RESET.getColor() + " × ";

    public ConsoleManager() {
        consoleCompleter = new ConsoleCompleter();
        lineReader = createLineReader();
        startThread();
    }

    private LineReader createLineReader(){

        Terminal terminal = null;
        try {
            System.setProperty("org.jline.terminal.dumb", "true");
            terminal = TerminalBuilder.builder().system(true).streams(System.in, System.out).encoding(Charsets.UTF_8).dumb(true).build();
        } catch (IOException exception) {
            exception.printStackTrace();
        }

        return LineReaderBuilder.builder()
                .completer(consoleCompleter)
                .terminal(terminal)
                .option(LineReader.Option.DISABLE_EVENT_EXPANSION, true)
                .option(LineReader.Option.AUTO_REMOVE_SLASH, false)
                .option(LineReader.Option.INSERT_TAB, false)
                .build();
    }

    public void startThread(){
        thread = new Thread(() -> {
            String line;
            while(!Thread.currentThread().isInterrupted()){
                line = lineReader.readLine(prompt);
                handleInput(line);
            }
        });
        thread.start();
    }

    private void handleInput(String input){

        if(input.isEmpty()){
            return;
        }

        if(CloudBootstrap.getBootstrap().isSetup()){
            return;
        }

        String[] args = input.split(" ");
        String command = args[0];
        if(CloudBootstrap.getBootstrap().getCommandManager().getCommandHandlerByName(command) == null){
            CloudBootstrap.getBootstrap().getLoggerProvider().info("The command could not be found!");
            return;
        }
        ICommandHandler commandHandler = CloudBootstrap.getBootstrap().getCommandManager().getCommandHandlerByName(command);
        commandHandler.handle(CloudBootstrap.getBootstrap().getConsoleSender(), args);
    }

    public void stopThread() {
        lineReader.getTerminal().reader().shutdown();
        lineReader.getTerminal().pause();
        thread.interrupt();
    }

    public LineReader getLineReader() {
        return lineReader;
    }

    public Thread getThread() {
        return thread;
    }

    public ConsoleCompleter getConsoleCompleter() {
        return consoleCompleter;
    }

    public String getPrefix() {
        return prompt;
    }
}
