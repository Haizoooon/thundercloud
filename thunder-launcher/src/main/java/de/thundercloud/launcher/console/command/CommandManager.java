package de.thundercloud.launcher.console.command;

/*

  » de.thundercloud.launcher.console.command

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:55

 */

import com.google.common.collect.Maps;
import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.exception.CommandFailureRegisterException;
import de.thundercloud.launcher.CloudBootstrap;
import org.reflections.Reflections;

import java.util.Map;
import java.util.Set;

public class CommandManager {

    private final Map<Command, ICommandHandler> commandHandlers;

    public CommandManager() {
        this.commandHandlers = Maps.newHashMap();
    }

    public void register(String packageName) {
        CloudBootstrap.getBootstrap().getLoggerProvider().debug(packageName);
        Reflections reflections = new Reflections(packageName);
        Set<Class<? extends ICommandHandler>> allClasses = reflections.getSubTypesOf(ICommandHandler.class);
        for (Class<? extends ICommandHandler> clazz : allClasses) {
            if (clazz.getAnnotation(Command.class) == null) {
                CloudBootstrap.getBootstrap().getLoggerProvider().severe("Command " + clazz.getName() + " cannot be registered");
                try {
                    throw new CommandFailureRegisterException();
                } catch (CommandFailureRegisterException e) {
                    e.printStackTrace();
                }
                return;
            }
            Command command = clazz.getAnnotation(Command.class);
            try {
                commandHandlers.put(command, clazz.newInstance());
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public ICommandHandler getCommandHandlerByName(String command) {
        return commandHandlers.get(commandHandlers.keySet().stream().filter(command1 -> command1.name().equalsIgnoreCase(command)).findAny().orElse(null));
    }

    public Map<Command, ICommandHandler> getCommandHandlers() {
        return commandHandlers;
    }
}
