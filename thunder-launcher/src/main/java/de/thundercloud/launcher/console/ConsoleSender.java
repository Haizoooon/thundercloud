package de.thundercloud.launcher.console;

/*

  » de.thundercloud.launcher.console

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:02

 */

import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.api.console.LogType;
import de.thundercloud.launcher.CloudBootstrap;

public class ConsoleSender implements ICommandSender {

    @Override
    public void sendMessage(String message) {
        CloudBootstrap.getBootstrap().getLoggerProvider().write(message);
    }

    @Override
    public void sendMessage(LogType logType, String message) {
        switch (logType){
            case DEBUG:
                CloudBootstrap.getBootstrap().getLoggerProvider().debug(message);
                break;
            case INFO:
                CloudBootstrap.getBootstrap().getLoggerProvider().info(message);
                break;
            case ERROR:
                CloudBootstrap.getBootstrap().getLoggerProvider().severe(message);
                break;
            case WARNING:
                CloudBootstrap.getBootstrap().getLoggerProvider().warning(message);
                break;
            case SETUP:
                CloudBootstrap.getBootstrap().getLoggerProvider().setup(message);
                break;
        }
    }


}
