package de.thundercloud.launcher.console.setup;

/*

  » de.thundercloud.launcher.console.setup

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 11:48

 */

import com.google.common.collect.Lists;
import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.launcher.console.setup.abstracts.SetupEnd;
import de.thundercloud.launcher.console.setup.abstracts.SetupInput;
import de.thundercloud.launcher.console.setup.interfaces.ISetup;

import java.util.List;

public class SetupBuilder {

    private final List<ISetup> setupQueue = Lists.newArrayList();

    private SetupInput currentInput;
    private final SetupEnd setupEnd;
    private final SetupInput[] setupInputs;
    private int currentIndex = 0;
    private final ISetup currentSetup;

    private final String prompt = CloudBootstrap.getBootstrap().getConsoleManager().getPrefix();

    public SetupBuilder(ISetup setup, SetupEnd setupEnd, SetupInput... setupInputs) {
        CloudBootstrap.getBootstrap().setSetup(true);
        this.currentSetup = setup;
        this.setupQueue.add(this.currentSetup);
        this.setupEnd = setupEnd;
        this.setupInputs = setupInputs;
        this.currentInput = setupInputs[currentIndex];
        CloudBootstrap.getBootstrap().getConsoleManager().getConsoleCompleter().setSuggestions(currentInput.getSuggestions());
        CloudBootstrap.getBootstrap().getLoggerProvider().clear();
        CloudBootstrap.getBootstrap().getLoggerProvider().setup(currentInput.getQuestion());
        nextQuestion(currentInput.handle(CloudBootstrap.getBootstrap().getConsoleManager().getLineReader().readLine(prompt)));
    }

    public void nextQuestion(boolean value){
        if(value){
            if(this.currentIndex == this.setupInputs.length - 1){
                CloudBootstrap.getBootstrap().setSetup(false);
                CloudBootstrap.getBootstrap().getLoggerProvider().clear();
                CloudBootstrap.getBootstrap().getLoggerProvider().getCachedMessages().forEach(message -> {
                    CloudBootstrap.getBootstrap().getLoggerProvider().write(message);
                });
                this.setupEnd.handle();
                this.setupQueue.remove(this.currentSetup);
                List<String> suggestions = Lists.newArrayList();
                CloudBootstrap.getBootstrap().getCommandManager().getCommandHandlers().keySet().forEach(command -> {
                    suggestions.add(command.name());
                });
                CloudBootstrap.getBootstrap().getConsoleManager().getConsoleCompleter().setSuggestions(suggestions);
                return;
            }
            this.currentIndex = this.currentIndex + 1;
            this.currentInput = this.setupInputs[this.currentIndex];
        }
        CloudBootstrap.getBootstrap().getConsoleManager().getConsoleCompleter().setSuggestions(currentInput.getSuggestions());
        CloudBootstrap.getBootstrap().getLoggerProvider().clear();
        CloudBootstrap.getBootstrap().getLoggerProvider().setup(currentInput.getQuestion());
        nextQuestion(currentInput.handle(CloudBootstrap.getBootstrap().getConsoleManager().getLineReader().readLine(prompt)));
    }

    public List<ISetup> getSetupQueue() {
        return setupQueue;
    }

    public SetupInput getCurrentInput() {
        return currentInput;
    }

    public SetupEnd getSetupEnd() {
        return setupEnd;
    }

    public SetupInput[] getSetupInputs() {
        return setupInputs;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public ISetup getCurrentSetup() {
        return currentSetup;
    }

    public String getPrompt() {
        return prompt;
    }
}
