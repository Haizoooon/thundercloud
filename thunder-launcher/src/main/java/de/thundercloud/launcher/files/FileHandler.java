/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.launcher.files;

/*

  » de.thundercloud.launcher.files

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 20:13

 */

import de.thundercloud.launcher.CloudBootstrap;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileHandler {

    public FileHandler() {
        deleteTempOrder();
    }

    public void deleteFiles(File folderName){
        if(folderName.exists()){
            try {
                FileUtils.deleteDirectory(folderName);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
    }

    public void deleteFile(String folder, String file){
        new File(folder, file).delete();
    }

    public void copyDirectory(String sourceDirectoryLocation, String destinationDirectoryLocation) {
        try {
            Files.walk(Paths.get(sourceDirectoryLocation))
                    .forEach(source -> {
                        Path destination = Paths.get(destinationDirectoryLocation, source.toString()
                                .substring(sourceDirectoryLocation.length()));
                        try {
                            Files.copy(source, destination);

                        } catch (IOException ignored) {

                        }
                    });
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void copyFileOutOfJar(File file, String name){
        InputStream inputStream = getClass().getResourceAsStream(name);
        File parent = file.getParentFile();
        parent.mkdirs();
        if(new File(name).exists()){
            return;
        }
        try {
            file.createNewFile();
            FileUtils.copyInputStreamToFile(inputStream, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void copyFileOutOfJar(File file, String name, InputStream inputStream){
        File parent = file.getParentFile();
        parent.mkdirs();
        if(new File(name).exists()){
            return;
        }
        try {
            file.createNewFile();
            FileUtils.copyInputStreamToFile(inputStream, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean createFile(String folderName, String fileName){
        try {
            return new File(folderName, fileName).createNewFile();
        } catch (IOException e) {
            CloudBootstrap.getBootstrap().getLoggerProvider().severe("Cannot create file '" + fileName + "' in folder '" + folderName + "'!");
            return false;
        }
    }

    public boolean copyFile(String from, String to){
        try {
            Files.copy(new File(from).toPath(), new File(to).toPath());
            return true;
        } catch (IOException e) {
            CloudBootstrap.getBootstrap().getLoggerProvider().severe("Cannot copy file '" + from + "' in '" + to + "'!");
            e.printStackTrace();
            return false;
        }
    }

    public boolean createFolder(String folderName){
        try {
            new File(folderName).mkdirs();
            return true;
        } catch (Exception e) {
            CloudBootstrap.getBootstrap().getLoggerProvider().severe("Cannot create folder  '" +  folderName + "'!");
            return false;
        }
    }

    public boolean folderExist(String folderName){
        return new File(folderName).exists();
    }

    public boolean fileExist(String folderName, String fileName){
        try {
            return new File(folderName, fileName).exists();
        } catch (Exception e) {
            CloudBootstrap.getBootstrap().getLoggerProvider().severe("Cannot check if file '" + fileName + "' in folder '" + folderName + "' is existing!");
            return false;
        }
    }

    public void createEula(String folder){
        File file = new File(folder, "eula.txt");
        try {
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write("eula=true\n");
            fileWriter.close();

        } catch (IOException ignored) { }
    }

    public void deleteTempOrder(){
        deleteFiles(new File("temp"));
    }
    
}
