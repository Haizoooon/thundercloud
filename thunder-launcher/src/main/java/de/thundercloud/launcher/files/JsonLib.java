/*
 * » Created by Niklas Sch. on 10.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.launcher.files;

import org.json.JSONArray;
import org.json.JSONObject;

public class JsonLib {

    private final JSONObject jsonObject;

    public JsonLib(){
        jsonObject = new JSONObject();
    }

    public JsonLib append(String property, Object value){
        jsonObject.put(property, value);
        return this;
    }

    public JsonLib appendObject(String object, String property, Object value){
        JSONObject jsonElement = jsonObject.getJSONObject(object);
        jsonElement.put(property, value);
        return this;
    }

    public JsonLib appendArray(String object, Object value){
        JSONArray jsonElement = jsonObject.getJSONArray(object);
        jsonElement.put(value);
        return this;
    }

    public String appendAll(){
        return jsonObject.toString();
    }

}

