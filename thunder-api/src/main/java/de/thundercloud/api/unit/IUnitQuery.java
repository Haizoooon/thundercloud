package de.thundercloud.api.unit;

/*

  » de.thundercloud.api.unit

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:07

 */

public interface IUnitQuery {

    void sendToAllTCP(String message);

    void sendQueryFromClient(String message);

}
