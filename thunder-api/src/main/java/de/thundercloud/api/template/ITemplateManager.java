package de.thundercloud.api.template;

/*

  » de.thundercloud.api.template

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 17:33

 */

import java.util.List;
import java.util.UUID;

public interface ITemplateManager {

    List<ITemplate> getTemplates();
    void createTemplate(String name, String staticType);
    boolean isTemplateExisting(String name);
    boolean isTemplateExisting(UUID uuid);

}
