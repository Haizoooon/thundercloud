package de.thundercloud.api.player;

/*

  » de.thundercloud.api.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 16:58

 */

import de.thundercloud.api.service.ICloudService;

import java.util.UUID;

public interface ICloudPlayer {

    /**
     * Sends the player a message
     * @param message
     */
    void sendMessage(String message);

    /**
     * Kicks the player with a specific reason
     * @param message
     */
    void kick(String message);

    /**
     * Kicks the player with default reason
     */
    void kick();

    void connect(ICloudService cloudService);

    /**
     * Sends the player a title
     * @param subTitle
     * @param title
     * @param fadeIn
     * @param stay
     * @param fadeOut
     */

    void sendTitle(String title, String subTitle, int fadeIn, int stay, int fadeOut);

    /**
     * Sends the player a message in the actionbar
     * @param value
     */
    void sendActionBar(String value);

    /**
     * Get the UUID from the player
     */
    UUID getUniqueId();

}
