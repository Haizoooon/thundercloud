/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.api.player;

/*

  » de.thundercloud.api.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 20:32

 */

import java.util.List;
import java.util.UUID;

public interface ICloudPlayerManager {

    /**
     * Get a cloud player by uniqueId
     * @param uuid
     * @return ICloudPlayer
     */
    ICloudPlayer getCachedCloudPlayer(UUID uuid);

    /**
     * Register a cloud player
     * @param uuid
     * @param name
     */
    void registerCloudPlayer(UUID uuid, String name);

    /**
     * Get all cached Cloud players
     */
    List<ICloudPlayer> getCachedCloudPlayers();

}
