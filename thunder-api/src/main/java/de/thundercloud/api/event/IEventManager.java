/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.api.event;

/*

  » de.thundercloud.api.event

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 20:22

 */

import java.util.List;

public interface IEventManager {

    void registerEvent(IEvent event);
    void callEvent(IEvent event);
    List<IEvent> getEvents();

}
