package de.thundercloud.api.wrapper;

/*

  » de.thundercloud.api.wrapper

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:42

 */

public enum WrapperStatus {

    CONNECTING,
    CONNECTED,
    DISCONNECTED;

}
