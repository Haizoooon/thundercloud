package de.thundercloud.api.wrapper;

/*

  » de.thundercloud.api.wrapper

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:41

 */

import java.util.List;

public interface IWrapperManager {

    void connect(IWrapper iWrapper);
    void disconnect(IWrapper iWrapper);
    List<IWrapper> getWrappers();

}
