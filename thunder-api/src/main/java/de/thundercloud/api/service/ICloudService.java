package de.thundercloud.api.service;

/*

  » de.thundercloud.api.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:44

 */

import com.esotericsoftware.kryonet.Connection;
import de.thundercloud.api.group.ICloudGroupService;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.api.service.process.ICloudServiceProcessManager;
import de.thundercloud.api.service.state.ServiceState;
import io.netty.channel.Channel;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public interface ICloudService {

    void start();
    void stop();

    String getName();
    String getServiceIdName();
    int getServiceId();
    int getPort();
    ICloudGroupService getGroupService();
    GroupVersion getGroupVersion();
    GroupType getGroupType();
    Connection getConnection();
    void setServiceState(ServiceState serviceState);
    ServiceState getServiceState();
    void setConnection(Connection connection);
    CopyOnWriteArrayList<String> getCachedScreenMessages();
    void setAuthenticated(boolean value);
    boolean isAuthenticated();
    int getOnlinePlayers();
    void setOnlinePlayers(int value);
    String getServiceStateString();
    ICloudServiceProcessManager getServiceProcessManager();

}
