/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.api.service.process;

/*

  » de.thundercloud.api.service.process

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 21:07

 */

import de.thundercloud.api.service.ICloudService;

public interface ICloudServiceProcessManager {

    void start();
    void stop();
    void executeCommand(String command);
    ICloudService getCloudService();

}
