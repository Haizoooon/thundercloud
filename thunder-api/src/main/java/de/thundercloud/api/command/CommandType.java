package de.thundercloud.api.command;

/*

  » de.thundercloud.api.command

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:37

 */

public enum CommandType {

    INGAME,
    CONSOLE,
    CONSOLE_AND_INGAME;

}

