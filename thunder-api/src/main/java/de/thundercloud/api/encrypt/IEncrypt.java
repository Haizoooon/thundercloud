package de.thundercloud.api.encrypt;

/*

  » de.thundercloud.api.encrypt

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:22

 */

public interface IEncrypt {

    String encrypt(String secretKey);
    String decrypt(String secretKey, String encryptedText);

}
