package de.thundercloud.api.packet;

import de.thundercloud.api.service.ICloudService;

public abstract class IPacket {

    private final String name;

    public IPacket(String name) {
        this.name = name;
    }

    public abstract void read(ICloudService cloudService, String value);
    public abstract void write(ICloudService cloudService, String value);

    public String getName() {
        return name;
    }
}
