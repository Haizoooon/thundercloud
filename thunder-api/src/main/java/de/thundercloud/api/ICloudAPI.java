package de.thundercloud.api;

/*

  » de.thundercloud.api

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:43

 */

import de.thundercloud.api.group.ICloudGroupService;
import de.thundercloud.api.group.ICloudGroupServiceManager;
import de.thundercloud.api.player.ICloudPlayerManager;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.api.service.ICloudServiceManager;
import de.thundercloud.api.wrapper.IWrapper;

import java.util.List;

public interface ICloudAPI {

    List<IWrapper> getCachedWrappers();
    List<ICloudService> getCachedCloudServices();
    List<ICloudGroupService> getCachedCloudGroupServices();
    ICloudPlayerManager getCloudPlayerManager();
    ICloudServiceManager getCloudServiceManager();
    ICloudGroupServiceManager getCloudGroupServiceManager();

}
