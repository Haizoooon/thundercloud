package de.thundercloud.api.connection;

/*

  » de.thundercloud.api.netty

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 01.04.2021 / 15:36

 */

public enum ConnectionProcess {

    /* PLAYER */

    PROXY_PLAYER_CONNECTED,
    PROXY_PLAYER_DISCONNECTED,
    PROXY_PLAYER_REGISTERED,
    PROXY_PLAYER_SEND_MESSAGE,
    PROXY_PLAYER_CONNECT_TO_SERVICE,
    PROXY_PLAYER_SEND_QUERY_TO_CONNECT_SERVER,

    /* CLOUDSERVICE */

    CLOUDSERVICE_UPDATE_EVENT,
    CLOUDSERVICE_REGISTERED,
    CLOUDSERVICE_UNREGISTERED,
    CLOUDSERVICE_SEND_TABLIST,
    CLOUDSERVICE_SEND_MOTD,
    CLOUDSERVICE_COPY,
    CLOUDSERVICE_SEND_UNIT,

    /* CLOUDGROUP */

    CLOUDGROUP_SEND_UNIT,

    /* MESSAGES */

    CLOUD_SEND_MESSAGES,

    /* MODULES */

    MODULE_UNIT_QUERY

}
