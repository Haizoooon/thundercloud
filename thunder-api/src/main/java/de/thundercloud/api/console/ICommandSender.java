package de.thundercloud.api.console;

/*

  » de.thundercloud.api.console

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 16:59

 */

public interface ICommandSender {

    void sendMessage(String message);
    void sendMessage(LogType logType, String message);

}
