package de.thundercloud.api.console;

/*

  » de.thundercloud.launcher.console.type

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 30.03.2021 / 20:58

 */

import de.thundercloud.api.console.Color;

public enum LogType {

    DEBUG("Debug", Color.BLUE),
    INFO("Info", Color.MAGENTA),
    ERROR("Error", Color.RED),
    WARNING("Warn", Color.YELLOW),
    SETUP("Setup", Color.CYAN);

    private String display;
    private Color colors;

    LogType(String display, Color colors) {
        this.display = display;
        this.colors = colors;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public Color getColor() {
        return colors;
    }

    public void setColors(Color colors) {
        this.colors = colors;
    }

}
