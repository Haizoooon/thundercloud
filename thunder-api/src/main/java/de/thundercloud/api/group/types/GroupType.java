package de.thundercloud.api.group.types;

/*

  » de.thundercloud.api.group.types

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 01.04.2021 / 14:50

 */

public enum GroupType {

    PROXY("Proxy"),
    LOBBY("Lobby"),
    SERVER("Server");

    String display;

    GroupType(String display) {
        this.display = display;
    }

    public String getDisplay() {
        return display;
    }
}
