package de.thundercloud.api.group;

/*

  » de.thundercloud.api.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:44

 */

import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;

public interface ICloudGroupService {

    String getName();
    int getMaxServers();
    int getMinServers();
    int getMaxMemory();
    int getPercentageToStartNewService();
    int getMaxPlayers();
    boolean isMaintenance();
    GroupType getGroupType();
    GroupVersion getGroupVersion();

}
