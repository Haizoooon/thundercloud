/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.api.group;

/*

  » de.thundercloud.api.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 12:20

 */

import java.util.List;

public interface ICloudGroupServiceManager {

    ICloudGroupService getCachedGroupService(String groupName);
    List<ICloudGroupService> getCachedGroupServices();

}
