/*
 * » Created by Niklas Sch. on 10.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.backend;

/*

  » de.thundercloud.plugin.backend

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.04.2021 / 20:49

 */

import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class ServerInfoHandler {

    private String serviceName;
    private int serviceId;
    private GroupType groupType;
    private GroupVersion groupVersion;

    public ServerInfoHandler() {
        register();
    }

    private void register(){
        try {
            File file = new File("THUNDERCLOUD.json");

            String content = new String(Files.readAllBytes(Paths.get(file.toURI())), StandardCharsets.UTF_8);

            JSONObject jsonObject = new JSONObject(content);

            serviceId = jsonObject.getInt("serviceId");
            serviceName = jsonObject.getString("serviceName");
            groupVersion = Arrays.stream(GroupVersion.values()).filter(version -> version.getDisplay().equalsIgnoreCase(jsonObject.getString("groupVersion"))).findAny().orElse(null);
            groupType = Arrays.stream(GroupType.values()).filter(type -> type.getDisplay().equalsIgnoreCase(jsonObject.getString("groupType"))).findAny().orElse(null);

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public int getServiceId() {
        return serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public GroupType getGroupType() {
        return groupType;
    }

    public GroupVersion getGroupVersion() {
        return groupVersion;
    }
}
