/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.backend.message;

/*

  » de.thundercloud.plugin.backend.message

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 15:22

 */

public class MessageContext {

    private String prefix;
    private String serviceStarted;
    private String serviceStopped;

    public MessageContext(String prefix, String serviceStarted, String serviceStopped) {
        this.prefix = prefix;
        this.serviceStarted = serviceStarted;
        this.serviceStopped = serviceStopped;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getServiceStarted() {
        return serviceStarted;
    }

    public void setServiceStarted(String serviceStarted) {
        this.serviceStarted = serviceStarted;
    }

    public String getServiceStopped() {
        return serviceStopped;
    }

    public void setServiceStopped(String serviceStopped) {
        this.serviceStopped = serviceStopped;
    }
}
