/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.backend.tablist;

/*

  » de.thundercloud.plugin.backend.tablist

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 11:26

 */

public class TablistConfiguration {

    private String footer, header;

    public TablistConfiguration(String footer, String header) {
        this.footer = footer;
        this.header = header;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getFooter() {
        return footer;
    }

    public String getHeader() {
        return header;
    }
}
