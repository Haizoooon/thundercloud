/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.spigot.listener;

/*

  » de.thundercloud.plugin.spigot.listener

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 09:49

 */

import de.thundercloud.api.player.ICloudPlayer;
import de.thundercloud.plugin.CloudAPI;
import de.thundercloud.plugin.spigot.SpigotBootstrap;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler
    public void handle(PlayerJoinEvent event){

        Player player = event.getPlayer();

        CloudAPI.getInstance().getCloudPlayerManager().registerCloudPlayer(player.getUniqueId(), player.getName());

        ICloudPlayer cloudPlayer = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(player.getUniqueId());

        Bukkit.getScheduler().runTaskLater(SpigotBootstrap.getPlugin(SpigotBootstrap.class), () -> {

            cloudPlayer.sendActionBar("HALLO DU LAPPEN");

        }, 20);

    }

}
