/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.spigot.impl;

/*

  » de.thundercloud.plugin.spigot.impl

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 20:25

 */

import de.thundercloud.plugin.spigot.title.TitleHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class SpigotPlayerHandler {

    public static void sendTitle(UUID uuid, String title, String subTitle, int fadeIn, int stay, int fadeOut){

        Player player = Bukkit.getPlayer(uuid);
        if(!player.isOnline()){
            return;
        }

        TitleHandler.sendTitle(player, fadeIn, stay, fadeOut, title, subTitle);

    }

    public static void sendActionbar(UUID uuid, String value){

        Player player = Bukkit.getPlayer(uuid);
        if(!player.isOnline()){
            return;
        }

        TitleHandler.sendActionbar(player, value);

    }

}
