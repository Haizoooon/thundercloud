/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.spigot.listener;

/*

  » de.thundercloud.plugin.spigot.listener

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 09:50

 */

import de.thundercloud.api.player.ICloudPlayer;
import de.thundercloud.plugin.CloudAPI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class PlayerCommandPreprocessListener implements Listener {

    @EventHandler
    public void handle(PlayerCommandPreprocessEvent event){
        String message = event.getMessage();
        Player player = event.getPlayer();

        if(message.startsWith("/rl") || message.startsWith("/reload")){
            if(player.hasPermission("bukkit.command.reload")){
                event.setCancelled(true);
                player.sendMessage("§8» §fThunderCloud §8× §7Cloud service §ccannot §7be reloaded");
                ICloudPlayer cloudPlayer = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(player.getUniqueId());
                cloudPlayer.connect(CloudAPI.getInstance().getCloudServiceManager().getCachedCloudService("BW-2x1-1"));
            }
        }

    }

}
