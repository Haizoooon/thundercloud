/*
 * » Created by Niklas Sch. on 10.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.spigot;

/*

  » de.thundercloud.plugin.spigot

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.04.2021 / 20:48

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.plugin.ThunderCloudModule;
import de.thundercloud.plugin.spigot.listener.PlayerCommandPreprocessListener;
import de.thundercloud.plugin.spigot.listener.PlayerJoinListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.JSONObject;

public class SpigotBootstrap extends JavaPlugin {

    private ThunderCloudModule thunderCloudModule;

    @Override
    public void onEnable() {

        thunderCloudModule = new ThunderCloudModule();
        thunderCloudModule.getCloudConnectorClient().sendUnitQuery(ConnectionProcess.CLOUDSERVICE_REGISTERED + ";" + thunderCloudModule.getServerInfoHandler().getServiceName() + "-" + thunderCloudModule.getServerInfoHandler().getServiceId());

        Bukkit.getPluginManager().registerEvents(new PlayerCommandPreprocessListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinListener(), this);

    }

    @Override
    public void onDisable() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("serviceName", ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceName());
        jsonObject.put("serviceId", ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceId());
        jsonObject.put("restartValue", true);

        thunderCloudModule.getCloudConnectorClient().sendUnitQuery(ConnectionProcess.CLOUDSERVICE_UNREGISTERED + ";" + jsonObject.toString());
        thunderCloudModule.getCloudConnectorClient().getClient().stop();
    }

    public ThunderCloudModule getThunderCloudModule() {
        return thunderCloudModule;
    }
}
