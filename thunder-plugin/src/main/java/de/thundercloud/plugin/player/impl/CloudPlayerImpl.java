/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.player.impl;

/*

  » de.thundercloud.plugin.player.impl

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 11:49

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.player.ICloudPlayer;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.plugin.ThunderCloudModule;
import de.thundercloud.plugin.player.CloudPlayerManager;
import de.thundercloud.plugin.proxy.bungee.impl.ProxyPlayerHandler;
import de.thundercloud.plugin.spigot.impl.SpigotPlayerHandler;
import org.json.JSONObject;

import java.util.List;
import java.util.UUID;

public class CloudPlayerImpl implements ICloudPlayer {

    private final UUID uuid;

    public CloudPlayerImpl(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void sendMessage(String message) {

    }

    @Override
    public void kick(String message) {

    }

    @Override
    public void kick() {

    }

    @Override
    public void connect(ICloudService cloudService) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("userName", CloudPlayerManager.getUserNameMap().get(this.uuid));
        jsonObject.put("serviceName", cloudService.getServiceIdName());
        ThunderCloudModule.getCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.PROXY_PLAYER_SEND_QUERY_TO_CONNECT_SERVER + ";" + jsonObject.toString());
    }

    @Override
    public void sendTitle(String title, String subTitle, int fadeIn, int stay, int fadeOut) {
        switch (ThunderCloudModule.getCloudModule().getServerInfoHandler().getGroupVersion()){
            case WATERFALL: case BUNGEECORD:
                ProxyPlayerHandler.sendTitle(this.uuid, title, subTitle, fadeIn, stay, fadeOut);
                break;
            case VELOCITY:

                break;

            default:
                SpigotPlayerHandler.sendTitle(uuid, title, subTitle, fadeIn, stay, fadeOut);
        }
    }

    @Override
    public void sendActionBar(String value) {
        switch (ThunderCloudModule.getCloudModule().getServerInfoHandler().getGroupVersion()){
            case WATERFALL: case BUNGEECORD:
                ProxyPlayerHandler.sendActionBar(uuid, value);
                break;
            case VELOCITY:

                break;

            default:
                SpigotPlayerHandler.sendActionbar(uuid, value);
        }
    }

    @Override
    public UUID getUniqueId() {
        return uuid;
    }

}
