/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.player;

/*

  » de.thundercloud.plugin.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 20:12

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.player.ICloudPlayer;
import de.thundercloud.api.player.ICloudPlayerManager;
import de.thundercloud.plugin.player.impl.CloudPlayerImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CloudPlayerManager implements ICloudPlayerManager {

    private static final List<ICloudPlayer> cachedCloudPlayers = Lists.newArrayList();
    private static final Map<UUID, ICloudPlayer> cloudPlayerMap = new HashMap<>();
    private static final Map<UUID, String> userNameMap = new HashMap<>();

    public ICloudPlayer getCachedCloudPlayer(UUID uuid) {
        return cloudPlayerMap.get(uuid);
    }

    public void registerCloudPlayer(UUID uuid, String name){
        if(!cloudPlayerMap.containsKey(uuid)){
            cloudPlayerMap.put(uuid, new CloudPlayerImpl(uuid));
            cachedCloudPlayers.add(cloudPlayerMap.get(uuid));
            userNameMap.put(uuid, name);
        }
    }

    @Override
    public List<ICloudPlayer> getCachedCloudPlayers() {
        return cachedCloudPlayers;
    }

    public static Map<UUID, String> getUserNameMap() {
        return userNameMap;
    }
}
