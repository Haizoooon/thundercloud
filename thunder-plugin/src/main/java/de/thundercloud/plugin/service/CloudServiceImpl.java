/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.service;

/*

  » de.thundercloud.plugin.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 21:07

 */

import com.esotericsoftware.kryonet.Connection;
import de.thundercloud.api.group.ICloudGroupService;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.api.service.process.ICloudServiceProcessManager;
import de.thundercloud.api.service.state.ServiceState;

import java.util.concurrent.CopyOnWriteArrayList;

public class CloudServiceImpl implements ICloudService {

    private String name;
    private int serviceId;
    private int port;
    private int onlinePlayers;
    private GroupVersion groupVersion;
    private GroupType groupType;
    private ServiceState serviceState;

    public CloudServiceImpl(String name, int serviceId, int port, int onlinePlayers, GroupVersion groupVersion, GroupType groupType, ServiceState serviceState) {
        this.name = name;
        this.serviceId = serviceId;
        this.port = port;
        this.onlinePlayers = onlinePlayers;
        this.groupVersion = groupVersion;
        this.groupType = groupType;
        this.serviceState = serviceState;
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getServiceIdName() {
        return name + "-" + serviceId;
    }

    @Override
    public int getServiceId() {
        return serviceId;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public ICloudGroupService getGroupService() {
        return null;
    }

    @Override
    public GroupVersion getGroupVersion() {
        return groupVersion;
    }

    @Override
    public GroupType getGroupType() {
        return groupType;
    }

    @Override
    public Connection getConnection() {
        return null;
    }

    @Override
    public void setServiceState(ServiceState serviceState) {

    }

    @Override
    public ServiceState getServiceState() {
        return null;
    }

    @Override
    public void setConnection(Connection connection) {

    }

    @Override
    public CopyOnWriteArrayList<String> getCachedScreenMessages() {
        return null;
    }

    @Override
    public void setAuthenticated(boolean value) {

    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public int getOnlinePlayers() {
        return onlinePlayers;
    }

    @Override
    public void setOnlinePlayers(int value) {

    }

    @Override
    public String getServiceStateString() {
        return null;
    }

    @Override
    public ICloudServiceProcessManager getServiceProcessManager() {
        return null;
    }
}
