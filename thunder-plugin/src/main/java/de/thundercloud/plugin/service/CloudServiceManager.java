/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.service;

/*

  » de.thundercloud.plugin.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 20:58

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.api.service.ICloudServiceManager;

import java.util.List;

public class CloudServiceManager implements ICloudServiceManager {

    private static final List<ICloudService> cachedCloudServices = Lists.newArrayList();

    @Override
    public ICloudService getCachedCloudService(String serviceName) {
        ICloudService cloudService = getCachedCloudServices().stream().filter(cachedCloudService -> cachedCloudService.getServiceIdName().equalsIgnoreCase(serviceName)).findAny().orElse(null);

        assert cloudService != null;

        if (!cachedCloudServices.contains(cloudService)) {
            cachedCloudServices.add(cloudService);
        }

        return cloudService;
    }

    @Override
    public List<ICloudService> getCachedCloudServices() {
        return cachedCloudServices;
    }
}
