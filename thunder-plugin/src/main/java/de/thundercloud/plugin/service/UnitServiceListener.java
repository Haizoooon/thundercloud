/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.service;

/*

  » de.thundercloud.plugin.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 21:01

 */

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.api.service.state.ServiceState;
import de.thundercloud.plugin.CloudAPI;
import org.json.JSONObject;

import java.util.Arrays;

public class UnitServiceListener extends Listener {

    @Override
    public void received(Connection connection, Object o) {

        if(o instanceof StringRequest){
            StringRequest stringRequest = (StringRequest) o;

            String[] args = stringRequest.value.split(";");

            ConnectionProcess connectionProcess = Arrays.stream(ConnectionProcess.values()).filter(process -> process.name().equalsIgnoreCase(args[0])).findAny().orElse(null);

            if (connectionProcess == null) {
                return;
            }

            if (connectionProcess.equals(ConnectionProcess.CLOUDSERVICE_SEND_UNIT)) {

                JSONObject jsonObject = new JSONObject(args[1]);
                String serviceName = jsonObject.getString("serviceName");
                int serviceId = jsonObject.getInt("serviceId");
                int port = jsonObject.getInt("port");
                GroupVersion groupVersion = Arrays.stream(GroupVersion.values()).filter(version -> version.getDisplay().equalsIgnoreCase(jsonObject.getString("groupVersion"))).findAny().orElse(null);
                GroupType groupType = Arrays.stream(GroupType.values()).filter(type -> type.getDisplay().equalsIgnoreCase(jsonObject.getString("groupType"))).findAny().orElse(null);
                ServiceState serviceState = Arrays.stream(ServiceState.values()).filter(state -> state.getDisplay().equalsIgnoreCase(jsonObject.getString("serviceState"))).findAny().orElse(null);
                int onlinePlayers = jsonObject.getInt("onlinePlayers");

                CloudAPI.getInstance().getCloudServiceManager().getCachedCloudServices().add(new CloudServiceImpl(serviceName, serviceId, port, onlinePlayers, groupVersion, groupType, serviceState));

            }

        }

    }
}
