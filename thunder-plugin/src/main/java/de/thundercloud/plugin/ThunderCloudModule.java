package de.thundercloud.plugin;

/*

  » de.thundercloud.plugin

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 15:50

 */

import de.thundercloud.plugin.backend.ServerInfoHandler;
import de.thundercloud.plugin.connection.CloudConnectorClient;

public class ThunderCloudModule {

    private static ThunderCloudModule cloudModule;
    private final CloudConnectorClient cloudConnectorClient = new CloudConnectorClient();
    private final ServerInfoHandler serverInfoHandler = new ServerInfoHandler();

    public ThunderCloudModule() {
        cloudModule = this;

        new CloudAPI();

    }

    public ServerInfoHandler getServerInfoHandler() {
        return serverInfoHandler;
    }

    public CloudConnectorClient getCloudConnectorClient() {
        return cloudConnectorClient;
    }

    public static ThunderCloudModule getCloudModule() {
        return cloudModule;
    }
}
