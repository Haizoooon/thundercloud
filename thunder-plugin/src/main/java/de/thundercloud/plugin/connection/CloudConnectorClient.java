package de.thundercloud.plugin.connection;

/*

  » de.thundercloud.plugin.connection

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 15:51

 */

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.api.response.StringResponse;
import de.thundercloud.plugin.ThunderCloudModule;
import de.thundercloud.plugin.group.unit.CloudGroupServiceListener;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import de.thundercloud.plugin.proxy.bungee.impl.ProxyPlayerHandler;
import de.thundercloud.plugin.service.UnitServiceListener;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.json.JSONObject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.UUID;

public class CloudConnectorClient {

    private Client client;

    public CloudConnectorClient() {
        connect();
    }

    public void connect(){
        try {

            client = new Client();
            client.start();
            client.connect(5019, "127.0.0.1", 40017, 40018);

            Kryo kryo = client.getKryo();
            kryo.register(StringRequest.class);
            kryo.register(StringResponse.class);

            client.addListener(new UnitServiceListener());
            client.addListener(new CloudGroupServiceListener());

            client.addListener(new Listener(){

                @Override
                public void received(Connection connection, Object o) {

                    if(o instanceof StringRequest){
                        StringRequest stringRequest = (StringRequest) o;

                        String[] args = stringRequest.value.split(";");

                        ConnectionProcess connectionProcess = Arrays.stream(ConnectionProcess.values()).filter(process -> process.name().equalsIgnoreCase(args[0])).findAny().orElse(null);

                        if (connectionProcess == null) {
                            return;
                        }

                        if(ThunderCloudModule.getCloudModule().getServerInfoHandler().getGroupVersion().equals(GroupVersion.BUNGEECORD)
                            || ThunderCloudModule.getCloudModule().getServerInfoHandler().getGroupVersion().equals(GroupVersion.WATERFALL)){
                            if(connectionProcess == ConnectionProcess.PROXY_PLAYER_CONNECT_TO_SERVICE){

                                JSONObject jsonObject = new JSONObject(args[1]);
                                String userName = jsonObject.getString("userName");
                                String serviceName = jsonObject.getString("serviceName");

                                BungeeModule.getModule().connect(userName, serviceName);

                            }
                        }


                        if (connectionProcess.equals(ConnectionProcess.CLOUD_SEND_MESSAGES)) {

                            JSONObject jsonObject = new JSONObject(args[1].replace("[", "").replace("]", ""));

                            BungeeModule.getModule().getMessageContext().setPrefix(jsonObject.getString("prefix"));
                            BungeeModule.getModule().getMessageContext().setServiceStarted(jsonObject.getString("serviceStarted"));
                            BungeeModule.getModule().getMessageContext().setServiceStopped(jsonObject.getString("serviceStopped"));

                        }

                        if (connectionProcess.equals(ConnectionProcess.CLOUDSERVICE_REGISTERED)) {

                            JSONObject jsonObject = new JSONObject(args[1].replace("[", "").replace("]", ""));

                            String name = jsonObject.getString("serviceName");
                            int serviceId = jsonObject.getInt("serviceId");
                            int port = jsonObject.getInt("port");
                            GroupType groupType = Arrays.stream(GroupType.values()).filter(type -> type.getDisplay().equalsIgnoreCase(jsonObject.getString("groupType"))).findAny().orElse(null);

                            if (groupType == null || groupType.equals(GroupType.PROXY)) {
                                return;
                            }

                            BungeeModule.getModule().registerService(name + "-" + serviceId, new InetSocketAddress("127.0.0.1", port));

                            TextComponent textComponent = new TextComponent("§8» §fThunderCloud §8× §7Cloud service §f" + name + "-" + serviceId + " §7was §astarted");
                            textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/server " + name + "-" + serviceId));

                            BungeeModule.getModule().getProxy().getPlayers().forEach(proxiedPlayer -> {
                                if(proxiedPlayer.hasPermission("thunder.see.started")){
                                    proxiedPlayer.sendMessage(textComponent);
                                }
                            });

                        }

                        if (connectionProcess.equals(ConnectionProcess.CLOUDSERVICE_UNREGISTERED)) {

                            JSONObject jsonObject = new JSONObject(args[1]);

                            String name = jsonObject.getString("serviceName");
                            int serviceId = jsonObject.getInt("serviceId");

                            ProxyServer.getInstance().getServers().remove(name + "-" + serviceId);
                            ProxyServer.getInstance().broadcast(new TextComponent("§8» §fThunderCloud §8× §7Cloud service §f" + name + "-" + serviceId + " §7was §cstopped§8!"));

                        }

                        if (connectionProcess.equals(ConnectionProcess.CLOUDSERVICE_SEND_TABLIST)) {

                            JSONObject jsonObject = new JSONObject(args[1]);

                            BungeeModule.getModule().getTablistConfiguration().setFooter(jsonObject.getString("footer"));
                            BungeeModule.getModule().getTablistConfiguration().setHeader(jsonObject.getString("header"));

                            BungeeModule.getModule().start();

                        }

                        if (connectionProcess.equals(ConnectionProcess.CLOUDSERVICE_SEND_MOTD)) {

                            JSONObject jsonObject = new JSONObject(args[1]);

                            BungeeModule.getModule().getMotdConfiguration().setLine1(jsonObject.getString("line-1"));
                            BungeeModule.getModule().getMotdConfiguration().setLine2(jsonObject.getString("line-2"));

                        }

                    }
                }

                @Override
                public void disconnected(Connection connection) {
                    sendUnitQuery(ConnectionProcess.CLOUDSERVICE_UNREGISTERED + ";" + ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceName() + "-" + ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceId());
                }
            });

        } catch (IOException exception) {
            exception.printStackTrace();
        }

    }

    public void sendUnitQuery(String message){
        StringRequest stringRequest = new StringRequest();
        stringRequest.value = message;
        client.sendTCP(stringRequest);
    }

    public Client getClient() {
        return client;
    }

}
