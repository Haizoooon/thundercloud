/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.proxy.bungee.commands.subcommands;

/*

  » de.thundercloud.plugin.proxy.bungee.commands.subcommands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 16:45

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.plugin.commands.interfaces.SubCommand;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class CopySubCommand extends SubCommand {

    public CopySubCommand() {
        super("copy");
    }

    @Override
    public void handle(ProxiedPlayer player, String[] args) {
        if(args.length == 2){
            String service = args[1];
            player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + "Trying to copy service §f" + service + "§8...");

            BungeeModule.getModule().getThunderCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.CLOUDSERVICE_COPY + ";" + service);

        }
    }

    @Override
    public String getUsage() {
        return null;
    }
}
