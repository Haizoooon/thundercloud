/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.proxy.bungee.commands;

/*

  » de.thundercloud.plugin.proxy.bungee.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 15:19

 */

import com.google.common.collect.Lists;
import de.thundercloud.plugin.commands.interfaces.SubCommand;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.List;

public class CloudCommand extends Command {

    public static List<SubCommand> subCommands = Lists.newArrayList();;

    public CloudCommand() {
        super("cloud");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {

        if(commandSender instanceof ProxiedPlayer){

            ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;

            if(args.length == 0){
                subCommands.forEach(subCommand -> {
                    proxiedPlayer.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + "§f/cloud §7" + subCommand.getCommand());
                });
                return;
            }

            if(subCommands.stream().anyMatch(subCommand -> subCommand.getCommand().equalsIgnoreCase(args[0]))){
                SubCommand subCommand = subCommands.stream().filter(subCommand1 -> subCommand1.getCommand().equalsIgnoreCase(args[0])).findAny().orElse(null);

                if(subCommand == null){
                    return;
                }

                subCommand.handle(proxiedPlayer, args);

            }

        }

    }
}
