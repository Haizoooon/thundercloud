/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.proxy.bungee.commands.subcommands;

/*

  » de.thundercloud.plugin.proxy.bungee.commands.subcommands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 21:18

 */

import de.thundercloud.plugin.CloudAPI;
import de.thundercloud.plugin.commands.interfaces.SubCommand;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ServiceSubCommand extends SubCommand {

    public ServiceSubCommand() {
        super("service");
    }

    @Override
    public void handle(ProxiedPlayer player, String[] args) {
        if(args.length == 2){
            if(args[0].equalsIgnoreCase("service")){
                if(args[1].equalsIgnoreCase("list")){
                    CloudAPI.getInstance().getCloudServiceManager().getCachedCloudServices().forEach(iCloudService -> {
                        player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + "§f" + iCloudService.getServiceIdName() + " #" + iCloudService.getPort());
                    });
                }
            }
        }
    }

    @Override
    public String getUsage() {
        return null;
    }
}
