/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.proxy.bungee.listener;

/*

  » de.thundercloud.plugin.proxy.bungee.listener

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 15:24

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.group.ICloudGroupService;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.plugin.CloudAPI;
import de.thundercloud.plugin.ThunderCloudModule;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.PendingConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerEvents implements Listener {

    @EventHandler
    public void handle(PostLoginEvent event){
        event.getPlayer().setReconnectServer(null);

        ICloudService lobbyService = CloudAPI.getInstance().getCloudServiceManager().getCachedCloudServices().stream().filter(iCloudService -> iCloudService.getGroupType().equals(GroupType.LOBBY)).findFirst().orElse(null);

        if(lobbyService == null){
            return;
        }

        if(ProxyServer.getInstance().getServerInfo(lobbyService.getServiceIdName()) == null){
            event.getPlayer().disconnect(new TextComponent("§cCloud service Lobby cannot be found!"));
        }
    }

    @EventHandler
    public void handle(ServerConnectEvent event){
        ICloudService lobbyService = CloudAPI.getInstance().getCloudServiceManager().getCachedCloudServices().stream().filter(iCloudService -> iCloudService.getGroupType().equals(GroupType.LOBBY)).findFirst().orElse(null);

        if(lobbyService == null){
            return;
        }
        ServerInfo serverInfo = (event.getTarget().getName().equals("fallback") ? ProxyServer.getInstance().getServerInfo(lobbyService.getServiceIdName()) : event.getTarget());
        event.setTarget(serverInfo);

        ProxiedPlayer proxiedPlayer = event.getPlayer();

        ICloudGroupService cloudGroupService = CloudAPI.getInstance().getCloudGroupServiceManager().getCachedGroupServices().stream().filter(iCloudGroupService -> iCloudGroupService.getGroupType().equals(GroupType.PROXY)).findAny().orElse(null);

        if(cloudGroupService == null){
            return;
        }

        if(cloudGroupService.isMaintenance()){
            if (proxiedPlayer.hasPermission("cloud.join")) {
                return;
            }
            proxiedPlayer.disconnect(new TextComponent("§8§m-----------------------\n\n§8» §fThunderCloud\n\n§7The server is in §cmaintenance\n§f§lASAP\n\n§8§m-----------------------"));
            event.setCancelled(true);
        }

    }

    @EventHandler
    public void handle(LoginEvent event){
        ThunderCloudModule.getCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.PROXY_PLAYER_CONNECTED + ";" + ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceName() + ";Player " + event.getConnection().getName() + " connected. (" + event.getConnection().getUniqueId() + "/" + event.getConnection().getAddress().getAddress().getHostAddress()+ "/" + ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceName() + ")");

        PendingConnection pendingConnection = event.getConnection();

        ThunderCloudModule.getCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.PROXY_PLAYER_REGISTERED + ";" + pendingConnection.getUniqueId() + ";" + pendingConnection.getName() + ";" + pendingConnection.getAddress().getAddress().getHostAddress() + ";" + pendingConnection.getAddress().getPort());

    }

    @EventHandler
    public void handle(PlayerDisconnectEvent event){
        ThunderCloudModule.getCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.PROXY_PLAYER_DISCONNECTED + ";" + ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceName() + ";Player " + event.getPlayer().getName() + " disconnected. (" + event.getPlayer().getUniqueId() + "/" + event.getPlayer().getAddress().getAddress().getHostAddress() + ")");
    }

    @EventHandler
    public void handle(ProxyPingEvent event){
        ServerPing serverPing = event.getResponse();

        ICloudGroupService cloudGroupService = CloudAPI.getInstance().getCloudGroupServiceManager().getCachedGroupServices().stream().filter(iCloudGroupService -> iCloudGroupService.getGroupType().equals(GroupType.PROXY)).findAny().orElse(null);

        if(cloudGroupService == null){
            return;
        }

        if(cloudGroupService.isMaintenance()){
            serverPing.setVersion(new ServerPing.Protocol("§8» §c§lMaintenance", -1));
        }

        serverPing.setDescriptionComponent(new TextComponent(BungeeModule.getModule().getMotdConfiguration().getLine1() + "\n" + BungeeModule.getModule().getMotdConfiguration().getLine2()));
    }

}
