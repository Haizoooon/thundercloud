/*
 * » Created by Niklas Sch. on 10.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.proxy.bungee;

/*

  » de.thundercloud.plugin.proxy.bungee

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.04.2021 / 18:01

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.plugin.ThunderCloudModule;
import de.thundercloud.plugin.backend.message.MessageContext;
import de.thundercloud.plugin.backend.motd.MotdConfiguration;
import de.thundercloud.plugin.backend.tablist.TablistConfiguration;
import de.thundercloud.plugin.proxy.bungee.commands.CloudCommand;
import de.thundercloud.plugin.proxy.bungee.commands.subcommands.CopySubCommand;
import de.thundercloud.plugin.proxy.bungee.commands.subcommands.ListSubCommand;
import de.thundercloud.plugin.proxy.bungee.commands.subcommands.ServiceSubCommand;
import de.thundercloud.plugin.proxy.bungee.listener.PlayerEvents;
import de.thundercloud.plugin.proxy.bungee.reconnect.ReconnectHandlerImpl;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;
import org.json.JSONObject;

import java.net.InetSocketAddress;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class BungeeModule extends Plugin {

    private ThunderCloudModule thunderCloudModule;
    private static BungeeModule module;
    private TablistConfiguration tablistConfiguration;
    private MotdConfiguration motdConfiguration;
    private MessageContext messageContext;


    @Override
    public void onLoad() {
        ProxyServer.getInstance().setReconnectHandler(new ReconnectHandlerImpl());
    }

    @Override
    public void onEnable() {
        module = this;

        thunderCloudModule = new ThunderCloudModule();
        thunderCloudModule.getCloudConnectorClient().sendUnitQuery(ConnectionProcess.CLOUDSERVICE_REGISTERED + ";" + thunderCloudModule.getServerInfoHandler().getServiceName() + "-" + thunderCloudModule.getServerInfoHandler().getServiceId());

        tablistConfiguration = new TablistConfiguration("", "");
        motdConfiguration = new MotdConfiguration("", "");
        messageContext = new MessageContext("", "", "");

        getProxy().getPluginManager().registerListener(this, new PlayerEvents());
        getProxy().getPluginManager().registerCommand(this, new CloudCommand());

        CloudCommand.subCommands.add(new ListSubCommand());
        CloudCommand.subCommands.add(new CopySubCommand());
        CloudCommand.subCommands.add(new ServiceSubCommand());

        ProxyServer.getInstance().getConfigurationAdapter().getServers().clear();
        ProxyServer.getInstance().getServers().clear();

        for(ListenerInfo listenerInfo : ProxyServer.getInstance().getConfigurationAdapter().getListeners()){
            listenerInfo.getServerPriority().clear();
        }

        registerFallback();

    }

    @Override
    public void onDisable() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("serviceName", ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceName());
        jsonObject.put("serviceId", ThunderCloudModule.getCloudModule().getServerInfoHandler().getServiceId());
        jsonObject.put("restartValue", true);

        thunderCloudModule.getCloudConnectorClient().sendUnitQuery(ConnectionProcess.CLOUDSERVICE_UNREGISTERED + ";" + jsonObject.toString());
        thunderCloudModule.getCloudConnectorClient().getClient().stop();
    }

    public void start(){
        getProxy().getScheduler().schedule(this, () -> {

            String footer = getTablistConfiguration().getFooter();
            String header = getTablistConfiguration().getHeader();

            getProxy().getPlayers().forEach(proxiedPlayer -> proxiedPlayer.setTabHeader(new TextComponent(replace(proxiedPlayer, header)), new TextComponent(replace(proxiedPlayer, footer))));

        }, 500, 500, TimeUnit.MILLISECONDS);
    }

    public void registerFallback(){
        registerService("fallback", new InetSocketAddress("127.0.0.1", 0));
    }

    public void registerService(String name, InetSocketAddress inetSocketAddress){
        ServerInfo serverInfo = ProxyServer.getInstance().constructServerInfo(name, inetSocketAddress, "A kyrocloud service", false);
        ProxyServer.getInstance().getServers().put(name, serverInfo);
    }

    private String replace(ProxiedPlayer proxiedPlayer, String line){
        return line.replace("%CLOUD_SERVICE_NAME%", proxiedPlayer.getServer().getInfo().getName()).replace("%ONLINE_PLAYERS%", String.valueOf(getProxy().getPlayers().size())).replace("%MAX_PLAYERS%", String.valueOf(50));
    }

    public void connect(String  userName, String serviceName){
        getProxy().getPlayer(userName).connect(ProxyServer.getInstance().getServerInfo(serviceName));
    }

    public MessageContext getMessageContext() {
        return messageContext;
    }

    public MotdConfiguration getMotdConfiguration() {
        return motdConfiguration;
    }

    public TablistConfiguration getTablistConfiguration() {
        return tablistConfiguration;
    }

    public static BungeeModule getModule() {
        return module;
    }

    public ThunderCloudModule getThunderCloudModule() {
        return thunderCloudModule;
    }
}
