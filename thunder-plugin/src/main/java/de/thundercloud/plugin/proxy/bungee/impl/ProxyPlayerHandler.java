/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.proxy.bungee.impl;

/*

  » de.thundercloud.plugin.proxy.bungee.impl

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 20:13

 */

import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.UUID;

public class ProxyPlayerHandler {

    public static void sendTitle(UUID uuid, String title, String subTitle, int fadeIn, int stay, int fadeOut){
        ProxiedPlayer proxiedPlayer = BungeeModule.getModule().getProxy().getPlayer(uuid);
        if(!proxiedPlayer.isConnected()){
            return;
        }
        Title titleHandler = ProxyServer.getInstance().createTitle();
        titleHandler.fadeIn(fadeIn);
        titleHandler.stay(stay);
        titleHandler.fadeOut(fadeOut);
        titleHandler.subTitle(new TextComponent(subTitle));
        titleHandler.title(new TextComponent(title));
        proxiedPlayer.sendTitle(titleHandler);

    }

    public static void sendActionBar(UUID uuid, String value){
        ProxiedPlayer proxiedPlayer = BungeeModule.getModule().getProxy().getPlayer(uuid);
        if(!proxiedPlayer.isConnected()){
            return;
        }
        proxiedPlayer.sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(value));
    }

}
