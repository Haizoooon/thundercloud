/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.proxy.bungee.commands.subcommands;

/*

  » de.thundercloud.plugin.proxy.bungee.commands.subcommands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 15:27

 */

import de.thundercloud.plugin.commands.interfaces.SubCommand;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ListSubCommand extends SubCommand {

    public ListSubCommand() {
        super("list");
    }

    @Override
    public void handle(ProxiedPlayer player, String[] args) {
        player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + "Nö");
    }

    @Override
    public String getUsage() {
        return null;
    }
}
