package de.thundercloud.plugin.commands.interfaces;

/*

  » de.stickmc.buildtools.commands.interfaces

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 27.03.2021 / 18:45

 */

import net.md_5.bungee.api.connection.ProxiedPlayer;

public abstract class SubCommand {

    private String command;

    public SubCommand(String command) {
        this.command = command;
    }

    public abstract void handle(ProxiedPlayer player, String[] args);
    public abstract String getUsage();

    public String getCommand() {
        return command;
    }
}
