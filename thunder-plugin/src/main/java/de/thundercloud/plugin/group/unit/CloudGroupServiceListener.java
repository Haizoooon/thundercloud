/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.group.unit;

/*

  » de.thundercloud.plugin.group.unit

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 12:58

 */

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.plugin.CloudAPI;
import de.thundercloud.plugin.group.impl.CloudGroupServiceImpl;
import org.json.JSONObject;

import java.util.Arrays;

public class CloudGroupServiceListener extends Listener {

    @Override
    public void received(Connection connection, Object o) {

        if(o instanceof StringRequest){
            StringRequest stringRequest = (StringRequest) o;
            String[] args = stringRequest.value.split(";");

            ConnectionProcess connectionProcess = Arrays.stream(ConnectionProcess.values()).filter(process -> process.name().equalsIgnoreCase(args[0])).findAny().orElse(null);

            if (connectionProcess == null) {
                return;
            }

            if(connectionProcess.equals(ConnectionProcess.CLOUDGROUP_SEND_UNIT)){

                JSONObject jsonObject = new JSONObject(args[1]);

                String name = jsonObject.getString("name");
                int maxServer = jsonObject.getInt("maxServer");
                int minServer = jsonObject.getInt("minServer");
                int maxMemory = jsonObject.getInt("maxMemory");
                int percentage = jsonObject.getInt("percentage");
                int maxPlayers = jsonObject.getInt("maxPlayers");
                boolean maintenance = jsonObject.getBoolean("maintenance");
                GroupVersion groupVersion = Arrays.stream(GroupVersion.values()).filter(version -> version.getDisplay().equalsIgnoreCase(jsonObject.getString("groupVersion"))).findAny().orElse(null);
                GroupType groupType = Arrays.stream(GroupType.values()).filter(type -> type.getDisplay().equalsIgnoreCase(jsonObject.getString("groupType"))).findAny().orElse(null);

                CloudAPI.getInstance().getCloudGroupServiceManager().getCachedGroupServices().add(new CloudGroupServiceImpl(name, maxServer, minServer, maxMemory, percentage, maxPlayers, maintenance, groupType, groupVersion));

            }

        }

    }

}
