/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.group.impl;

/*

  » de.thundercloud.plugin.group.impl

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 12:17

 */

import de.thundercloud.api.group.ICloudGroupService;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;

public class CloudGroupServiceImpl implements ICloudGroupService {

    private final String name;
    private final int maxServers, minServers, maxMemory, percentageToStartNewService, maxPlayers;
    private final boolean maintenance;
    private final GroupType groupType;
    private final GroupVersion groupVersion;

    public CloudGroupServiceImpl(String name, int maxServers, int minServers, int maxMemory, int percentageToStartNewService, int maxPlayers, boolean maintenance, GroupType groupType, GroupVersion groupVersion) {
        this.name = name;
        this.maxServers = maxServers;
        this.minServers = minServers;
        this.maxMemory = maxMemory;
        this.percentageToStartNewService = percentageToStartNewService;
        this.maxPlayers = maxPlayers;
        this.maintenance = maintenance;
        this.groupType = groupType;
        this.groupVersion = groupVersion;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getMaxServers() {
        return maxServers;
    }

    @Override
    public int getMinServers() {
        return minServers;
    }

    @Override
    public int getMaxMemory() {
        return maxMemory;
    }

    @Override
    public int getPercentageToStartNewService() {
        return percentageToStartNewService;
    }

    @Override
    public int getMaxPlayers() {
        return maxPlayers;
    }

    @Override
    public boolean isMaintenance() {
        return maintenance;
    }

    @Override
    public GroupType getGroupType() {
        return groupType;
    }

    @Override
    public GroupVersion getGroupVersion() {
        return groupVersion;
    }
}
