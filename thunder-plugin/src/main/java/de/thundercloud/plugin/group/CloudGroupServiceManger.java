/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin.group;

/*

  » de.thundercloud.plugin.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 12:17

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.group.ICloudGroupService;
import de.thundercloud.api.group.ICloudGroupServiceManager;

import java.util.List;

public class CloudGroupServiceManger implements ICloudGroupServiceManager {

    private static final List<ICloudGroupService> cachedGroupServices = Lists.newArrayList();

    @Override
    public ICloudGroupService getCachedGroupService(String groupName) {
        ICloudGroupService cloudGroupService = cachedGroupServices.stream().filter(iCloudGroupService -> iCloudGroupService.getName().equalsIgnoreCase(groupName)).findAny().orElse(null);

        assert cloudGroupService != null;

        if (!cachedGroupServices.contains(cloudGroupService)) {
            cachedGroupServices.add(cloudGroupService);
        }

        return cloudGroupService;
    }

    @Override
    public List<ICloudGroupService> getCachedGroupServices() {
        return cachedGroupServices;
    }

}
