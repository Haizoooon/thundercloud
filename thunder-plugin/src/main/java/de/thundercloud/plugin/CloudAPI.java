/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 10.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.plugin;

/*

  » de.thundercloud.api

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.04.2021 / 11:32

 */

import de.thundercloud.api.ICloudAPI;
import de.thundercloud.api.group.ICloudGroupService;
import de.thundercloud.api.group.ICloudGroupServiceManager;
import de.thundercloud.api.player.ICloudPlayerManager;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.api.service.ICloudServiceManager;
import de.thundercloud.api.wrapper.IWrapper;
import de.thundercloud.plugin.group.CloudGroupServiceManger;
import de.thundercloud.plugin.player.CloudPlayerManager;
import de.thundercloud.plugin.service.CloudServiceManager;

import java.util.ArrayList;
import java.util.List;

public class CloudAPI implements ICloudAPI {

    private static final List<IWrapper> cachedWrappers = new ArrayList<>();
    private static final List<ICloudService> cachedCloudServices = new ArrayList<>();
    private static final List<ICloudGroupService> cachedCloudGroupServices = new ArrayList<>();
    private static final ICloudPlayerManager cloudPlayerManager = new CloudPlayerManager();
    private static final ICloudServiceManager cloudServiceManager = new CloudServiceManager();
    private static final ICloudGroupServiceManager cloudGroupServiceManager = new CloudGroupServiceManger();

    private static CloudAPI instance;

    public CloudAPI() {
        instance = this;
    }

    public static CloudAPI getInstance() {
        return instance;
    }

    @Override
    public List<IWrapper> getCachedWrappers() {
        return cachedWrappers;
    }

    @Override
    public List<ICloudService> getCachedCloudServices() {
        return cachedCloudServices;
    }

    @Override
    public List<ICloudGroupService> getCachedCloudGroupServices() {
        return cachedCloudGroupServices;
    }

    @Override
    public ICloudPlayerManager getCloudPlayerManager() {
        return cloudPlayerManager;
    }

    @Override
    public ICloudServiceManager getCloudServiceManager() {
        return cloudServiceManager;
    }

    @Override
    public ICloudGroupServiceManager getCloudGroupServiceManager() {
        return cloudGroupServiceManager;
    }

}
