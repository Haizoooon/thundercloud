/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.config;

/*

  » de.thundercloud.module.config

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 20:21

 */

public class Config {

    private String prefix, alreadyConnected, successConnected;

    public Config(String prefix, String alreadyConnected, String successConnected) {
        this.prefix = prefix;
        this.alreadyConnected = alreadyConnected;
        this.successConnected = successConnected;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void setAlreadyConnected(String alreadyConnected) {
        this.alreadyConnected = alreadyConnected;
    }

    public void setSuccessConnected(String successConnected) {
        this.successConnected = successConnected;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getAlreadyConnected() {
        return alreadyConnected;
    }

    public String getSuccessConnected() {
        return successConnected;
    }
}
