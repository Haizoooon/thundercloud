/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module;

/*

  » de.thundercloud.module

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 20:14

 */

import de.thundercloud.api.module.Module;
import de.thundercloud.base.CloudBase;
import de.thundercloud.launcher.console.logging.LoggerProvider;
import de.thundercloud.launcher.external.reader.JsonReader;
import de.thundercloud.module.config.Config;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Module(name = "hub-module", reloadable = true, version = "1.0-SNAPSHOT", authors = {"Haizoooon"})
public class HubModule {

    private static HubModule module;

    private LoggerProvider loggerProvider;
    private Config config;

    public void onInitialization(LoggerProvider loggerProvider){
        module = this;

        this.loggerProvider = loggerProvider;
        this.config = new Config("", "", "");

        if(!CloudBase.getBase().getFileHandler().fileExist("modules/hub-module", "config.json")){
            CloudBase.getBase().getFileHandler().createFile("modules/hub-module", "config.json");
            CloudBase.getBase().getFileHandler().copyFileOutOfJar(new File("modules/hub-module", "config.json"), "/files/config.json", getClass().getResourceAsStream("/files/config.json"));
        }

        try {
            String content = new String(Files.readAllBytes(Paths.get(new File("modules/hub-module", "config.json").toURI())), StandardCharsets.UTF_8);
            JsonReader jsonReader = new JsonReader(content);
            config.setPrefix(String.valueOf(jsonReader.read("prefix")));
            config.setAlreadyConnected(String.valueOf(jsonReader.read("alreadyHub")));
            config.setSuccessConnected(String.valueOf(jsonReader.read("connected")));



        } catch (IOException exception) {
            exception.printStackTrace();
        }

    }

    public Config getConfig() {
        return config;
    }

    public LoggerProvider getLoggerProvider() {
        return loggerProvider;
    }

    public static HubModule getModule() {
        return module;
    }
}
