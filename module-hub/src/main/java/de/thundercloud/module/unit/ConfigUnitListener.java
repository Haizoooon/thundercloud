/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.unit;

/*

  » de.thundercloud.module.unit

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 20:30

 */

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.module.HubModule;
import org.json.JSONObject;

import java.util.Arrays;

public class ConfigUnitListener extends Listener {

    @Override
    public void received(Connection connection, Object o) {
        if(o instanceof StringRequest) {
            StringRequest stringRequest = (StringRequest) o;
            String value = stringRequest.value;

            String[] args = value.split(";");

            ConnectionProcess connectionProcess = Arrays.stream(ConnectionProcess.values()).filter(process -> process.name().equalsIgnoreCase(args[0].toUpperCase())).findAny().orElse(null);

            if (connectionProcess == null) {
                return;
            }

            if(connectionProcess.equals(ConnectionProcess.MODULE_UNIT_QUERY)){

                JSONObject jsonObject = new JSONObject(args[1].replace("[", "").replace("]", ""));

                if(jsonObject.getString("type").equalsIgnoreCase("hubModuleConfig")){
                    //TODO: change messages
                }

            }

        }
    }
}
