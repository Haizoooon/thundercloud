/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.proxy.bungee.commands;

/*

  » de.thundercloud.module.proxy.bungee.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 20:17

 */

import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.player.ICloudPlayer;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.plugin.CloudAPI;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class HubCommand extends Command {

    public HubCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {

        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) commandSender;

        ICloudService cloudService = CloudAPI.getInstance().getCloudServiceManager().getCachedCloudServices().stream().filter(iCloudService -> iCloudService.getGroupType().equals(GroupType.LOBBY)).findAny().orElse(null);

        ICloudPlayer cloudPlayer = CloudAPI.getInstance().getCloudPlayerManager().getCachedCloudPlayer(proxiedPlayer.getUniqueId());

        if(cloudService == null){
            return;
        }

        if (proxiedPlayer.getServer().getInfo().getName().contains(cloudService.getName())) {
            proxiedPlayer.sendMessage("§8» §fProxy §8× §7You are already connected to a §flobby");
            return;
        }

        cloudPlayer.connect(cloudService);

    }
}
