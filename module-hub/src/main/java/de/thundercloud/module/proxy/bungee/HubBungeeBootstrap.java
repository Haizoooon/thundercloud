/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.proxy.bungee;

/*

  » de.thundercloud.module.proxy.bungee

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 20:17

 */

import de.thundercloud.module.proxy.bungee.commands.HubCommand;
import net.md_5.bungee.api.plugin.Plugin;

public class HubBungeeBootstrap extends Plugin {

    @Override
    public void onEnable() {
        getProxy().getPluginManager().registerCommand(this, new HubCommand("lobby"));
        getProxy().getPluginManager().registerCommand(this, new HubCommand("l"));
        getProxy().getPluginManager().registerCommand(this, new HubCommand("hub"));
    }
}
