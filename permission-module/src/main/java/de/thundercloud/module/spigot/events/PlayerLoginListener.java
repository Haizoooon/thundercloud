/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.spigot.events;

/*

  » de.thundercloud.module.spigot.events

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 16:48

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.player.IPermissionPlayer;
import de.thundercloud.module.pool.PermissionPool;
import de.thundercloud.module.spigot.permissible.BukkitCloudPermissibleBase;
import de.thundercloud.module.spigot.reflection.ReflectionUtils;
import de.thundercloud.plugin.spigot.SpigotBootstrap;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;

import java.lang.reflect.Field;


public class PlayerLoginListener implements Listener {

    @EventHandler
    public void handle(PlayerLoginEvent event){
        SpigotBootstrap.getPlugin(SpigotBootstrap.class).getThunderCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.MODULE_UNIT_QUERY + "#" + "registerPlayer#" + event.getPlayer().getUniqueId() + "#" + event.getPlayer().getName());

    }

    @EventHandler
    public void handle(PlayerJoinEvent event){
        try {
            Class<?> clazz = ReflectionUtils.reflectClass(".entity.CraftHumanEntity");
            Field field = null;
            if(clazz != null){
                field = clazz.getDeclaredField("perm");
            }
            if(field == null){
                return;
            }
            field.setAccessible(true);
            field.set(event.getPlayer(), new BukkitCloudPermissibleBase(event.getPlayer()));
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
        }

        Player player = event.getPlayer();
        IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPlayer(player.getUniqueId());
        for(IPermissionGroup permissionGroup : permissionPlayer.getAllGroups()){
            player.sendMessage(permissionGroup.getName());
        }

    }

}
