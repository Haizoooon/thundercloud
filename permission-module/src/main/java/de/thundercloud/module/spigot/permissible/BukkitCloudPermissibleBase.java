/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.spigot.permissible;

/*

  » de.thundercloud.module.spigot.permissible

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 16:49

 */

import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.player.IPermissionPlayer;
import de.thundercloud.module.pool.PermissionPool;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;

import java.util.List;

public class BukkitCloudPermissibleBase extends PermissibleBase {

    private final Player player;
    private final IPermissionPlayer permissionPlayer;

    public BukkitCloudPermissibleBase(Player player) {
        super(player);
        this.player = player;
        this.permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPlayer(player.getUniqueId());
    }

    @Override
    public boolean isPermissionSet(String name) {
        List<IPermissionGroup> permissionGroups = permissionPlayer.getAllGroups();
        return permissionGroups.stream().anyMatch(permissionGroup -> permissionGroup.getPermissions().stream().anyMatch(permission -> permission.getPermission().equalsIgnoreCase(name) || permission.getPermission().equalsIgnoreCase("*")));
    }

    @Override
    public boolean isPermissionSet(Permission perm) {
        List<IPermissionGroup> permissionGroups = permissionPlayer.getAllGroups();
        return permissionGroups.stream().anyMatch(permissionGroup -> permissionGroup.getPermissions().stream().anyMatch(permission -> permission.getPermission().equalsIgnoreCase(perm.getName()) || permission.getPermission().equalsIgnoreCase("*")));
    }

    @Override
    public boolean hasPermission(String inName) {
        List<IPermissionGroup> permissionGroups = permissionPlayer.getAllGroups();
        return permissionGroups.stream().anyMatch(permissionGroup -> permissionGroup.getPermissions().stream().anyMatch(permission -> permission.getPermission().equalsIgnoreCase(inName) || permission.getPermission().equalsIgnoreCase("*")));
    }

    @Override
    public boolean hasPermission(Permission perm) {
        List<IPermissionGroup> permissionGroups = permissionPlayer.getAllGroups();
        return permissionGroups.stream().anyMatch(permissionGroup -> permissionGroup.getPermissions().stream().anyMatch(permission -> permission.getPermission().equalsIgnoreCase(perm.getName()) || permission.getPermission().equalsIgnoreCase("*")));
    }

    @Override
    public boolean isOp() {
        List<IPermissionGroup> permissionGroups = permissionPlayer.getAllGroups();
        return permissionGroups.stream().anyMatch(permissionGroup -> permissionGroup.getPermissions().stream().anyMatch(permission -> permission.getPermission().equalsIgnoreCase("*")));
    }

    @Override
    public void recalculatePermissions() {

    }
}
