/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.spigot;

/*

  » de.thundercloud.module.spigot

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 16:48

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.module.spigot.events.PlayerLoginListener;
import de.thundercloud.module.unit.PermissionUnitListener;
import de.thundercloud.plugin.spigot.SpigotBootstrap;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class SpigotModule extends JavaPlugin {

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new PlayerLoginListener(), this);

        SpigotBootstrap.getPlugin(SpigotBootstrap.class).getThunderCloudModule().getCloudConnectorClient().getClient().addListener(new PermissionUnitListener());

        SpigotBootstrap.getPlugin(SpigotBootstrap.class).getThunderCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.MODULE_UNIT_QUERY + "#" + "getAllGroups");

    }

}
