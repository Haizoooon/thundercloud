/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module;

/*

  » de.thundercloud.module

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:40

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.module.Module;
import de.thundercloud.base.CloudBase;
import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.launcher.files.FileHandler;
import de.thundercloud.launcher.console.logging.LoggerProvider;
import de.thundercloud.module.commands.console.PermsCommand;
import de.thundercloud.module.sql.PermissionAdapter;
import de.thundercloud.module.unit.PermissionUnitServerListener;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;

import java.io.File;

@Module(name = "permission-module", reloadable = true, authors = {"Haizoooon", "YyTFlo"}, version = "1.0-SNAPSHOT")
public class PermissionModule {

    private static PermissionModule module;
    private LoggerProvider loggerProvider;
    private PermissionAdapter permissionAdapter;

    public void onInitialization(LoggerProvider loggerProvider){
        module = this;

        this.loggerProvider = loggerProvider;
        this.permissionAdapter = new PermissionAdapter();

        FileHandler fileHandler = CloudBase.getBase().getFileHandler();
        fileHandler.createFolder("modules/permission-module/groups");
        fileHandler.copyFileOutOfJar(new File("modules/permission-module/groups", "admin.json"), "/default/admin.json");

        CloudBase.getBase().getCloudConnectorServer().getServer().addListener(new PermissionUnitServerListener());

        PermsCommand permsCommand = new PermsCommand();
        Command command = permsCommand.getClass().getAnnotation(Command.class);
        CloudBootstrap.getBootstrap().getCommandManager().getCommandHandlers().put(command, permsCommand);

    }

    public PermissionAdapter getPermissionAdapter() {
        return permissionAdapter;
    }

    public LoggerProvider getLoggerProvider() {
        return loggerProvider;
    }

    public static PermissionModule getModule() {
        return module;
    }
}
