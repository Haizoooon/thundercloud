/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.pool;

/*

  » de.thundercloud.module.pool

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 17:51

 */

import de.thundercloud.module.manager.GroupManager;
import de.thundercloud.module.player.PermissionPlayerManager;

public class PermissionPool {

    private static final GroupManager groupManager = new GroupManager();
    private static final PermissionPlayerManager permissionPlayerManager = new PermissionPlayerManager();

    private static final PermissionPool instance = new PermissionPool();

    public static PermissionPool getInstance() {
        return instance;
    }

    public PermissionPlayerManager getPermissionPlayerManager() {
        return permissionPlayerManager;
    }

    public GroupManager getGroupManager() {
        return groupManager;
    }

}
