/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.sql;

/*

  » de.thundercloud.module.sql

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 18:33

 */

import com.google.common.collect.Lists;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.database.SqlAdapter;
import de.thundercloud.base.manager.database.types.SqlDataType;
import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.group.PermissionGroup;
import de.thundercloud.module.permission.Permission;

import java.util.Arrays;
import java.util.List;

public class PermissionAdapter {

    private final String table = "cloud_permissions";
    private final String[] keys = new String[]{"groupName", "permissions"};
    private final SqlDataType[] sqlDataTypes = new SqlDataType[]{SqlDataType.VARCHAR, SqlDataType.TEXT};
    private final SqlAdapter sqlAdapter;

    public PermissionAdapter() {
        this.sqlAdapter = CloudBase.getBase().getDatabaseHandler().getSqlAdapter();
        createTable();
        addDefaults();
    }

    public void createTable(){
        this.sqlAdapter.createTable(table, sqlAdapter.getTableInformation(keys, sqlDataTypes));
    }

    public void addDefaults(){
        if(!sqlAdapter.existsInTable(table, keys[0], "default")){
            sqlAdapter.addMoreInTable(table, Arrays.asList(keys), Arrays.asList("Admin", "*;"));
            sqlAdapter.addMoreInTable(table, Arrays.asList(keys), Arrays.asList("default", "test;"));
        }
    }

    public void addPermToGroup(IPermissionGroup permissionGroup, Permission permission){
        String value = getPermsFromGroup(permissionGroup);
        String stringBuilder = value + ";" + permission.getPermission();
        setPermsFromGroup(permissionGroup, stringBuilder);
    }

    public void removePermission(IPermissionGroup permissionGroup, Permission permission) {
        List<Permission> permissions = getPermissionsGroup(permissionGroup);
        permissions.remove(permission);
        StringBuilder stringBuilder = new StringBuilder();
        for(Permission perm : permissions){
            stringBuilder.append(perm.getPermission()).append(";");
        }
        setPermsFromGroup(permissionGroup, stringBuilder.toString());
    }

    public void addGroup(String groupName) {
        if(!groupExits(groupName)) sqlAdapter.addMoreInTable(table, Arrays.asList(keys), Arrays.asList(groupName, " "));
    }

    public boolean groupExits(String groupName) {
        return sqlAdapter.existsInTable(table, keys[0], groupName);
    }

    public void setPermsFromGroup(IPermissionGroup permissionGroup, String value){
        sqlAdapter.getSqlBaseExecutor().executeUpdate("DELETE FROM " + table + " WHERE groupName = '" + permissionGroup.getName() + "';");
        sqlAdapter.addMoreInTable(table, Arrays.asList(keys), Arrays.asList(permissionGroup.getName(), value));
    }

    public String getPermsFromGroup(IPermissionGroup permissionGroup){
        return String.valueOf(sqlAdapter.getFromTable(table, keys[0], permissionGroup.getName(), keys[1]));
    }

    public String getPermsFromGroup(String groupName){
        return String.valueOf(sqlAdapter.getFromTable(table, keys[0], groupName, keys[1]));
    }

    public List<Permission> getPermissionsGroup(IPermissionGroup permissionGroup){
        List<Permission> permissions = Lists.newArrayList();
        String[] permissionsSplit = getPermsFromGroup(permissionGroup).split(";");
        for(String string : permissionsSplit){
            permissions.add(new Permission(string, -1, true));
        }
        return permissions;
    }

    public List<Permission> getPermissionsGroup(String groupName){
        List<Permission> permissions = Lists.newArrayList();
        String[] permissionsSplit = getPermsFromGroup(groupName).split(";");
        for(String string : permissionsSplit){
            permissions.add(new Permission(string, -1, true));
        }
        return permissions;
    }

    public List<IPermissionGroup> getPermissionGroups(){
        List<IPermissionGroup> list = sqlAdapter.getSqlBaseExecutor().executeQuery("SELECT * FROM " + table, resultSet -> {
            List<IPermissionGroup> content = Lists.newArrayList();
            while(resultSet.next()){
                content.add(new PermissionGroup(resultSet.getString("groupName"), getPermissionsGroup(resultSet.getString("groupName"))));
            }
            return content;
        }, Lists.newArrayList());
        return !list.isEmpty() ? list : Lists.newArrayList();
    }

}
