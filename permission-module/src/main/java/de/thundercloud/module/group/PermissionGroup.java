/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.group;

/*

  » de.thundercloud.module.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:55

 */

import de.thundercloud.module.permission.Permission;

import java.util.List;

public class PermissionGroup implements IPermissionGroup{

    private final String name;
    private final List<Permission> permissions;

    public PermissionGroup(String name, List<Permission> permissions) {
        this.name = name;
        this.permissions = permissions;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Permission> getPermissions() {
        return permissions;
    }
}
