/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.player;

/*

  » de.thundercloud.module.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 11:04

 */

import java.util.List;
import java.util.UUID;

public interface IPermissionPlayerManager {

    IPermissionPlayer getCachedPlayer(UUID uuid);
    IPermissionPlayer getCachedPermissionPlayer(UUID uuid);
    void createPlayer(UUID uuid);
    List<IPermissionPlayer> getCachedPermissionPlayers();

}
