/*
 * » Created by Niklas Sch. on 14.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.player.impl;

/*

  » de.thundercloud.module.player.impl

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 14.04.2021 / 11:06

 */

import com.google.common.collect.Lists;
import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.permission.Permission;
import de.thundercloud.module.player.IPermissionPlayer;
import de.thundercloud.module.pool.PermissionPool;

import java.util.List;
import java.util.UUID;

public class PermissionPlayerImpl implements IPermissionPlayer {

    private final UUID uuid;
    private final List<IPermissionGroup> groups = Lists.newArrayList();
    private final List<Permission> permissions = Lists.newArrayList();

    public PermissionPlayerImpl(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public UUID getUniqueId() {
        return uuid;
    }

    @Override
    public IPermissionGroup getHighestGroup() {
        return PermissionPool.getInstance().getPermissionPlayerManager().getPermissionGroupList().get(this.uuid).get(0);
    }

    @Override
    public List<IPermissionGroup> getAllGroups() {
        return PermissionPool.getInstance().getPermissionPlayerManager().getPermissionGroupList().get(this.uuid);
    }

    @Override
    public List<Permission> getPermissions() {
        return permissions;
    }

}
