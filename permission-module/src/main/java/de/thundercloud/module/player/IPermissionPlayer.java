/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.player;

/*

  » de.thundercloud.module.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:44

 */

import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.permission.Permission;

import java.util.List;
import java.util.UUID;

public interface IPermissionPlayer {

    UUID getUniqueId();
    IPermissionGroup getHighestGroup();
    List<IPermissionGroup> getAllGroups();
    List<Permission> getPermissions();

}
