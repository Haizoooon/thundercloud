/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.player;

/*

  » de.thundercloud.module.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:59

 */

import com.google.common.collect.Lists;
import de.thundercloud.module.PermissionModule;
import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.permission.Permission;
import de.thundercloud.module.player.impl.PermissionPlayerImpl;
import de.thundercloud.module.pool.PermissionPool;

import java.util.*;

public class PermissionPlayerManager implements IPermissionPlayerManager {

    private static final List<IPermissionPlayer> cachedPermissionPlayers = Lists.newArrayList();
    private static final Map<UUID, IPermissionPlayer> cachedPlayers = new HashMap<>();
    private static final Map<UUID, List<IPermissionGroup>> permissionGroupList = new HashMap<>();

    @Override
    public IPermissionPlayer getCachedPlayer(UUID uuid) {
        return cachedPlayers.get(uuid);
    }

    @Override
    public IPermissionPlayer getCachedPermissionPlayer(UUID uuid){
        return cachedPlayers.get(uuid);
    }

    @Override
    public void createPlayer(UUID uuid){
        cachedPermissionPlayers.remove(cachedPlayers.get(uuid));
        cachedPlayers.remove(uuid);
        permissionGroupList.remove(uuid);

        cachedPlayers.put(uuid, new PermissionPlayerImpl(uuid));
        cachedPermissionPlayers.add(cachedPlayers.get(uuid));
        permissionGroupList.put(uuid, Lists.newArrayList());
    }

    public Map<UUID, List<IPermissionGroup>> getPermissionGroupList() {
        return permissionGroupList;
    }

    public Map<UUID, IPermissionPlayer> getCachedPlayers() {
        return cachedPlayers;
    }

    @Override
    public List<IPermissionPlayer> getCachedPermissionPlayers() {
        return cachedPermissionPlayers;
    }
}
