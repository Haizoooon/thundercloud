/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.permission;

/*

  » de.thundercloud.module.permission

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:42

 */

public class Permission {

    private final String permission;
    private final long timeOut;
    private final boolean active;

    public Permission(String permission, long timeOut, boolean active) {
        this.permission = permission;
        this.timeOut = timeOut;
        this.active = active;
    }

    public boolean isExpired(){
        return (System.currentTimeMillis() > this.timeOut && this.timeOut != -1);
    }

    public String getPermission() {
        return permission;
    }

    public long getTimeOut() {
        return timeOut;
    }

    public boolean isActive() {
        return active;
    }
}
