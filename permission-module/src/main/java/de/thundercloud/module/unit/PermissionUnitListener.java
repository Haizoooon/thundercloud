/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.unit;

/*

  » de.thundercloud.module.unit

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 17:54

 */

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.google.common.collect.Lists;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.group.PermissionGroup;
import de.thundercloud.module.permission.Permission;
import de.thundercloud.module.player.IPermissionPlayer;
import de.thundercloud.module.player.PermissionPlayerManager;
import de.thundercloud.module.pool.PermissionPool;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class PermissionUnitListener extends Listener {

    @Override
    public void received(Connection connection, Object o) {

        if(o instanceof StringRequest){
            StringRequest stringRequest = (StringRequest) o;
            String value = stringRequest.value;

            String[] args = value.split("#");

            ConnectionProcess connectionProcess = Arrays.stream(ConnectionProcess.values()).filter(process -> process.name().equalsIgnoreCase(args[0].toUpperCase())).findAny().orElse(null);

            if(connectionProcess == null){
                return;
            }

            if(connectionProcess == ConnectionProcess.MODULE_UNIT_QUERY){

                JSONObject jsonObject = new JSONObject(args[1]);

                if(jsonObject.getString("type").equalsIgnoreCase("group")){

                    String stringValue = jsonObject.getString("value");

                    String groupName = stringValue.split(";")[0];
                    List<Permission> permissions = Lists.newArrayList();
                    for(String permission : stringValue.split(";")){
                        permissions.add(new Permission(permission, -1, true));
                    }
                    PermissionPool.getInstance().getGroupManager().addGroup(new PermissionGroup(groupName, permissions));

                } else if(jsonObject.getString("type").equalsIgnoreCase("playerRegistered")){

                    String uniqueId = jsonObject.getString("uniqueId");

                    PermissionPool.getInstance().getPermissionPlayerManager().createPlayer(UUID.fromString(uniqueId));

                    IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPlayer(UUID.fromString(uniqueId));

                    List<IPermissionGroup> permissionGroups = Lists.newArrayList();

                    String[] split = jsonObject.getString("value").split(";");

                    for(String string : split){
                        IPermissionGroup permissionGroup = PermissionPool.getInstance().getGroupManager().getGroups().stream().filter(permissionGroup1 -> permissionGroup1.getName().equalsIgnoreCase(string)).findAny().orElse(null);

                        if(permissionGroup == null){
                            return;
                        }

                        permissionGroups.add(permissionGroup);

                    }

                    for(IPermissionGroup permissionGroup : permissionGroups){
                        permissionPlayer.getAllGroups().clear();
                        permissionPlayer.getAllGroups().add(permissionGroup);
                    }

                    PermissionPool.getInstance().getPermissionPlayerManager().getPermissionGroupList().put(UUID.fromString(uniqueId), permissionGroups);

                }

            }

        }

    }

}
