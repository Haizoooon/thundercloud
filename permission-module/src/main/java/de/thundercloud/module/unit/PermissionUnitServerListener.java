/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 13.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.unit;

/*

  » de.thundercloud.module.unit

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 13.04.2021 / 16:23

 */

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.google.common.collect.Lists;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.base.CloudBase;
import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.module.PermissionModule;
import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.group.PermissionGroup;
import de.thundercloud.module.permission.Permission;
import de.thundercloud.module.pool.PermissionPool;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class PermissionUnitServerListener extends Listener {

    @Override
    public void received(Connection connection, Object o) {
        if (o instanceof StringRequest) {
            StringRequest stringRequest = (StringRequest) o;

            String value = stringRequest.value;

            String[] args = value.split("#");

            ConnectionProcess connectionProcess = Arrays.stream(ConnectionProcess.values()).filter(process -> process.name().equalsIgnoreCase(args[0].toUpperCase())).findAny().orElse(null);

            if (connectionProcess == null) {
                return;
            }

            if (connectionProcess == ConnectionProcess.MODULE_UNIT_QUERY) {
                if (args[1].equalsIgnoreCase("getAllGroups")) {

                    PermissionModule.getModule().getPermissionAdapter().getPermissionGroups().forEach(permissionGroup -> {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("type", "group");
                        StringRequest stringRequest1 = new StringRequest();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append(permissionGroup.getName());
                        stringBuilder.append(";");
                        for (Permission permission : permissionGroup.getPermissions()) {
                            stringBuilder.append(permission.getPermission()).append(";");
                        }
                        jsonObject.put("value", stringBuilder.toString());
                        stringRequest1.value = ConnectionProcess.MODULE_UNIT_QUERY + "#" + jsonObject.toString();

                        CloudBase.getBase().getCloudConnectorServer().getServer().sendToAllTCP(stringRequest1);
                    });

                }

                if (args[1].equalsIgnoreCase("registerPlayer")) {

                    String uniqueId = args[2];
                    String name = args[3];

                    JSONObject content = new JSONObject(CloudBase.getBase().getPlayerAdapter().getJsonString(name));

                    JSONArray jsonArray = content.getJSONArray("groups");

                    JSONObject sendObject = new JSONObject();

                    sendObject.put("type", "playerRegistered");
                    sendObject.put("uniqueId", uniqueId);

                    StringBuilder stringBuilder = new StringBuilder();

                    for(Object object : jsonArray.toList()){
                        stringBuilder.append(object.toString()).append(";");
                    }

                    sendObject.put("value", stringBuilder.toString());

                    StringRequest stringRequest1 = new StringRequest();
                    stringRequest1.value = ConnectionProcess.MODULE_UNIT_QUERY + "#" + sendObject.toString();

                    CloudBase.getBase().getCloudConnectorServer().getServer().sendToAllTCP(stringRequest1);

                }

            }

        }
    }
}
