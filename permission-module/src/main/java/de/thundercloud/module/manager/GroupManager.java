/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.manager;

/*

  » de.thundercloud.module.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:45

 */

import com.google.common.collect.Lists;
import de.thundercloud.module.group.IPermissionGroup;

import java.util.List;

public class GroupManager {

    private final List<IPermissionGroup> groups = Lists.newArrayList();

    public void addGroup(IPermissionGroup permissionGroup){
        groups.add(permissionGroup);
    }

    public List<IPermissionGroup> getGroups() {
        return groups;
    }
}
