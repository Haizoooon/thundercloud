/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.commands.console;

/*

  » de.thundercloud.module.commands.console

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 19:50

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.api.console.LogType;
import de.thundercloud.module.PermissionModule;
import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.permission.Permission;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "perms", type = CommandType.CONSOLE)
public class PermsCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender iCommandSender, String[] args) {
        if(args.length == 2) {
            if(args[1].equalsIgnoreCase("groups")) {
                iCommandSender.sendMessage(LogType.INFO, "All groups:");
                PermissionModule.getModule().getPermissionAdapter().getPermissionGroups().forEach(iPermissionGroup -> iCommandSender.sendMessage(LogType.INFO, iPermissionGroup.getName()));
            }
        } else if(args.length == 4) {
            if(args[1].equalsIgnoreCase("group")){
                if(args[2].equalsIgnoreCase("create")){
                    String groupName = args[3];

                    if(PermissionModule.getModule().getPermissionAdapter().groupExits(groupName)) {
                        iCommandSender.sendMessage(LogType.INFO, "The group §c" + groupName + "§r already exists!");
                        return;
                    }

                    PermissionModule.getModule().getPermissionAdapter().addGroup(groupName);
                    iCommandSender.sendMessage(LogType.INFO, "The group §a" + groupName + "§r has been created!");

                }
            }
        } else if(args.length == 5) {
//            if(args[1].equalsIgnoreCase("user")) {
//                if(args[3].equalsIgnoreCase("add")) {
//                    if(args[4].equalsIgnoreCase("permission") || args[4].equalsIgnoreCase("perm")) {
//                        String permission = args[5];
//                        IPermissionPlayer player = PermissionPool.getPermissionPlayerManager().getCachedPlayer();
//
//                        if(player == null){
//                            iCommandSender.sendMessage(LogType.INFO, "The player §c" + permission + "§r dosen't exists!");
//                            return;
//                        }
//
//                        PermissionModule.getModule().getPermissionAdapter().addPermToGroup(group, new Permission(permission, -1, true));
//                        iCommandSender.sendMessage(LogType.INFO, "The Permission §a" + permission + "§r has been added to the group §a" + group.getName() + "§r!");
//
//                    }
//                } else if(args[3].equalsIgnoreCase("remove")) {
//
//                }
//            }
        } else if(args.length == 6){
            if(args[1].equalsIgnoreCase("group")){
                if(args[3].equalsIgnoreCase("add")){
                    if(args[4].equalsIgnoreCase("permission") || args[4].equalsIgnoreCase("perm")){
                        String permission = args[5];
                        IPermissionGroup group = PermissionModule.getModule().getPermissionAdapter().getPermissionGroups().stream().filter(iPermissionGroup -> iPermissionGroup.getName().equalsIgnoreCase(args[2])).findAny().orElse(null);

                        if(group == null){
                            iCommandSender.sendMessage(LogType.INFO, "The group §c" + group.getName() + "§r dosen't exists!");
                            return;
                        }

                        PermissionModule.getModule().getPermissionAdapter().addPermToGroup(group, new Permission(permission, -1, true));
                        iCommandSender.sendMessage(LogType.INFO, "The Permission §a" + permission + "§r has been added to the group §a" + group.getName() + "§r!");
                    }
                } else if(args[3].equalsIgnoreCase("remove")) {
                    if(args[4].equalsIgnoreCase("permission") || args[4].equalsIgnoreCase("perm")) {
                        String permission = args[5];
                        IPermissionGroup group = PermissionModule.getModule().getPermissionAdapter().getPermissionGroups().stream().filter(iPermissionGroup -> iPermissionGroup.getName().equalsIgnoreCase(args[2])).findAny().orElse(null);

                        if(group == null){
                            iCommandSender.sendMessage(LogType.INFO, "The group §c" + group.getName() + "§r dosen't exists!");
                            return;
                        }

                        PermissionModule.getModule().getPermissionAdapter().removePermission(group, new Permission(permission, -1, true));
                        iCommandSender.sendMessage(LogType.INFO, "The Permission §a" + permission + "§r has been removed to the group §a" + group.getName() + "§r!");

                    }
                }
            }
        }

    }

    @Override
    public String[] getUsage() {
        return new String[]{"perms » Show the help of perms"};
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

}
