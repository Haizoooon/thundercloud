/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.commands.ingame;

import de.thundercloud.module.PermissionModule;
import de.thundercloud.module.pool.PermissionPool;
import de.thundercloud.plugin.commands.interfaces.SubCommand;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PermsCommand extends SubCommand {

    public PermsCommand() {
        super("perms");
    }

    @Override
    public void handle(ProxiedPlayer player, String[] args) {
        if(args.length == 2){
            if (args[0].equalsIgnoreCase("perms")) {
                switch (args[1]){
                    case "groups":
                        player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + " ");
                        player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + "All groups§8:");
                        PermissionPool.getInstance().getGroupManager().getGroups().forEach(permissionGroup -> {
                            player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + "§f" + permissionGroup.getName());
                            player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + " ");
                        });
                        player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + " ");
                        break;
                    case "list":

                        PermissionPool.getInstance().getPermissionPlayerManager().getCachedPermissionPlayers().forEach(iPermissionPlayer -> {
                            player.sendMessage(BungeeModule.getModule().getMessageContext().getPrefix() + iPermissionPlayer.getUniqueId());
                        });

                        break;
                }
            }
        }
    }

    @Override
    public String getUsage() {
        return null;
    }
}
