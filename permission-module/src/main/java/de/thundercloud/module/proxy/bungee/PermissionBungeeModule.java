/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.proxy.bungee;

/*

  » de.thundercloud.module.proxy.bungee

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:58

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.module.commands.ingame.PermsCommand;
import de.thundercloud.module.proxy.bungee.events.PermissionCheckListener;
import de.thundercloud.module.unit.PermissionUnitListener;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import de.thundercloud.plugin.proxy.bungee.commands.CloudCommand;
import net.md_5.bungee.api.plugin.Plugin;

public class PermissionBungeeModule extends Plugin {

    @Override
    public void onEnable() {

        getProxy().getPluginManager().registerListener(this, new PermissionCheckListener());

        CloudCommand.subCommands.add(new PermsCommand());

        BungeeModule.getModule().getThunderCloudModule().getCloudConnectorClient().getClient().addListener(new PermissionUnitListener());

        BungeeModule.getModule().getThunderCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.MODULE_UNIT_QUERY + "#" + "getAllGroups");

    }

}
