/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.module.proxy.bungee.events;

/*

  » de.thundercloud.module.proxy.bungee.events

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:58

 */

import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.module.group.IPermissionGroup;
import de.thundercloud.module.player.IPermissionPlayer;
import de.thundercloud.module.pool.PermissionPool;
import de.thundercloud.plugin.proxy.bungee.BungeeModule;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PermissionCheckEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PermissionCheckListener implements Listener {

    @EventHandler
    public void handle(PostLoginEvent event){

        ProxiedPlayer proxiedPlayer = event.getPlayer();

        BungeeModule.getModule().getThunderCloudModule().getCloudConnectorClient().sendUnitQuery(ConnectionProcess.MODULE_UNIT_QUERY + "#registerPlayer#" + proxiedPlayer.getUniqueId() + "#" + proxiedPlayer.getName());

    }

    @EventHandler
    public void handle(PermissionCheckEvent event){
        ProxiedPlayer proxiedPlayer = (ProxiedPlayer) event.getSender();
        IPermissionPlayer permissionPlayer = PermissionPool.getInstance().getPermissionPlayerManager().getCachedPlayer(proxiedPlayer.getUniqueId());
        IPermissionGroup permissionGroup = permissionPlayer.getHighestGroup();

        event.setHasPermission(permissionGroup.getPermissions().stream().anyMatch(permission -> permission.getPermission().equalsIgnoreCase(event.getPermission()) || permission.getPermission().equalsIgnoreCase("*") || permission.getPermission().equalsIgnoreCase("'*'")));

    }

}
