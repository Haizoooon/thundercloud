package de.thundercloud.base.manager.service;

/*

  » de.thundercloud.base.manager.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 01.04.2021 / 15:02

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.group.CloudGroupService;
import de.thundercloud.launcher.CloudBootstrap;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class CloudServiceManager {

    private static final CopyOnWriteArrayList<ICloudService> cloudServices = new CopyOnWriteArrayList<>();

    public void addAllServices(){
        for (CloudGroupService cloudGroupService : CloudBase.getBase().getCloudGroupServiceManager().getCloudGroupServiceList()) {
            for(int i = 0; i < cloudGroupService.getMaxServers(); i++){
                addService(i + 1, cloudGroupService);
            }
        }
    }

    public void addService(int serviceId, CloudGroupService cloudGroupService){
        CloudService cloudService = new CloudService(cloudGroupService.getName(), serviceId, cloudGroupService.getGroupType(), cloudGroupService.getGroupVersion());
        cloudServices.add(cloudService);
    }

    public void startAllServices(){
        for(ICloudService cloudService : getCloudServices()){
            cloudService.start();
        }
    }

    public CopyOnWriteArrayList<ICloudService> getCloudServices() {
        return cloudServices;
    }
}
