package de.thundercloud.base.manager.database;

/*

  » de.thundercloud.base.manager.database

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:58

 */

import de.thundercloud.base.manager.database.security.AccessLoader;
import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.launcher.console.logging.LoggerProvider;

public class DatabaseHandler {

    private final SqlExecutor sqlExecutor;
    private final AccessLoader accessLoader;
    private final SqlConnector sqlConnector;
    private final SqlAdapter sqlAdapter;

    public DatabaseHandler() {

        this.accessLoader = new AccessLoader();
        this.sqlConnector = new SqlConnector();

        LoggerProvider loggerProvider = CloudBootstrap.getBootstrap().getLoggerProvider();

        loggerProvider.info("Trying to connect to mysql...");

        if(accessLoader.isAccessJsonExist()){
            this.sqlConnector.connect(this.accessLoader.loadObject());
        } else {

        }

        this.sqlExecutor = new SqlExecutor(this.sqlConnector.getConnection());
        this.sqlAdapter = new SqlAdapter(this.sqlExecutor);

    }

    public void disconnect(){
        if(accessLoader.isAccessJsonExist())
            sqlConnector.disconnect();
    }

    public SqlExecutor getSqlExecutor() {
        return sqlExecutor;
    }

    public AccessLoader getAccessLoader() {
        return accessLoader;
    }

    public SqlConnector getSqlConnector() {
        return sqlConnector;
    }

    public SqlAdapter getSqlAdapter() {
        return sqlAdapter;
    }
}
