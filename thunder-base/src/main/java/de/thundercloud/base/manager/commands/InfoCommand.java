/*
 * » Created by Niklas Sch. on 10.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.manager.commands;

/*

  » de.thundercloud.base.manager.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 10.04.2021 / 18:20

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.api.console.LogType;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.base.CloudBase;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "info", type = CommandType.CONSOLE)
public class InfoCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender iCommandSender, String[] args) {

        if(args.length == 2){
            String serviceName = args[1];

            ICloudService cloudService = CloudBase.getBase().getCloudServiceManager().getCloudServices().stream().filter(service -> service.getServiceIdName().equalsIgnoreCase(serviceName)).findAny().orElse(null);

            if(cloudService == null){
                return;
            }

            iCommandSender.sendMessage(LogType.INFO, " ");
            iCommandSender.sendMessage(LogType.INFO, "State » " + cloudService.getServiceStateString());
            iCommandSender.sendMessage(LogType.INFO, " ");

        }

    }

    @Override
    public String[] getUsage() {
        return new String[]{"info » Info about a service"};
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }

}
