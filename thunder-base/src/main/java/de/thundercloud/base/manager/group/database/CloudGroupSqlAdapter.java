package de.thundercloud.base.manager.group.database;

/*

  » de.thundercloud.base.manager.service.database

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 13:29

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.database.SqlAdapter;
import de.thundercloud.base.manager.database.types.SqlDataType;
import de.thundercloud.base.manager.group.CloudGroupService;

import java.util.Arrays;
import java.util.List;

public class CloudGroupSqlAdapter {

    private final String table = "cloud_groups";
    private final String[] keys = new String[]{"name", "maxServers", "minServers", "maxMemory", "percentageToStartNewService", "maxPlayers", "maintenance", "groupType", "groupVersion"};
    private final SqlDataType[] databaseTypes = new SqlDataType[]{SqlDataType.VARCHAR, SqlDataType.INT, SqlDataType.INT, SqlDataType.INT, SqlDataType.INT, SqlDataType.INT, SqlDataType.VARCHAR, SqlDataType.VARCHAR, SqlDataType.VARCHAR};
    private final SqlAdapter sqlAdapter;

    public CloudGroupSqlAdapter() {
        this.sqlAdapter = CloudBase.getBase().getDatabaseHandler().getSqlAdapter();
        createTable();
    }

    public void createTable(){
        this.sqlAdapter.createTable(table, sqlAdapter.getTableInformation(keys, databaseTypes));
    }

    public void createGroup(String name, int maxServers, int minServers, int maxMemory, int percentage, int maxPlayers, boolean maintenance, GroupType groupType, GroupVersion groupVersion){
        sqlAdapter.addMoreInTable(table, Arrays.asList(keys), Arrays.asList(name, maxServers, minServers, maxMemory, percentage, maxPlayers, String.valueOf(maintenance), groupType.getDisplay(), groupVersion.getDisplay()));
    }

    public List<CloudGroupService> getAllGroupServices(){
        List<CloudGroupService> list = sqlAdapter.getSqlBaseExecutor().executeQuery("SELECT * FROM " + table, resultSet -> {
            List<CloudGroupService> content = Lists.newArrayList();
            while(resultSet.next()){
                String name = resultSet.getString(keys[0]);
                int maxServers = resultSet.getInt(keys[1]);
                int minServers = resultSet.getInt(keys[2]);
                int maxMemory = resultSet.getInt(keys[3]);
                int percentage = resultSet.getInt(keys[4]);
                int maxPlayers = resultSet.getInt(keys[5]);
                String maintenance = resultSet.getString(keys[6]);
                String groupType = resultSet.getString(keys[7]);
                String groupVersion = resultSet.getString(keys[8]);
                content.add(new CloudGroupService(name, maxServers, minServers, maxMemory, percentage, maxPlayers, Boolean.parseBoolean(maintenance), Arrays.stream(GroupType.values()).filter(group -> group.getDisplay().equalsIgnoreCase(groupType)).findAny().orElse(null), Arrays.stream(GroupVersion.values()).filter(version -> version.getDisplay().equalsIgnoreCase(groupVersion)).findAny().orElse(null)));
            }
            return content;
        }, Lists.newArrayList());
        return !list.isEmpty() ? list : Lists.newArrayList();
    }

}
