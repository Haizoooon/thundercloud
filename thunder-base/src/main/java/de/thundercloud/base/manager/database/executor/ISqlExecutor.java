package de.thundercloud.base.manager.database.executor;

/*

  » de.thundercloud.base.manager.database.executor

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:47

 */

import de.thundercloud.base.manager.database.function.ISqlFunction;

import java.sql.ResultSet;

public interface ISqlExecutor {

    <T> T executeQuery(String query, ISqlFunction<ResultSet, T> function, T defaultValue);
    int executeUpdate(String query);

}
