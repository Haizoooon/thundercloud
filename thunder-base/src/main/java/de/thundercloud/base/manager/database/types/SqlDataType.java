package de.thundercloud.base.manager.database.types;

/*

  » de.thundercloud.base.manager.database.types

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:53

 */

public enum SqlDataType {

    TEXT("text"),
    VARCHAR("varchar", 64),
    CHAR("char", 255),
    INT("int"),
    LONGBLOB("LONGBLOB");

    private final String sqlTag;
    private int length;

    SqlDataType(String sqlTag) {
        this.sqlTag = sqlTag;
    }

    SqlDataType(String sqlTag, int length){
        this.sqlTag = sqlTag;
        this.length = length;
    }

    public int getLength() {
        return length;
    }

    public SqlDataType setLength(int length) {
        this.length = length;
        return this;
    }

    public String getSqlTag() {
        if(this == SqlDataType.VARCHAR) return sqlTag + "(" + length + ")";
        else return sqlTag;
    }

}
