package de.thundercloud.base.manager.setups;

/*

  » de.thundercloud.base.manager.setups

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 18:04

 */

import de.thundercloud.base.manager.database.DatabaseHandler;
import de.thundercloud.launcher.console.setup.SetupBuilder;
import de.thundercloud.launcher.console.setup.abstracts.SetupEnd;
import de.thundercloud.launcher.console.setup.abstracts.SetupInput;
import de.thundercloud.launcher.console.setup.interfaces.ISetup;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SqlSetup implements ISetup {

    private String address, port, database, username, password;

    public SqlSetup(DatabaseHandler databaseHandler) {
        new SetupBuilder(this, new SetupEnd() {
            @Override
            public void handle() {

                databaseHandler.getAccessLoader().createSqlAccess(address, Integer.parseInt(port), database, username, password);
                databaseHandler.getSqlConnector().connect(databaseHandler.getAccessLoader().loadObject());

            }
        }, new SetupInput("Which host do you want to use") {
            @Override
            public List<String> getSuggestions() {
                return Arrays.asList("localhost", "127.0.0.1");
            }

            @Override
            public boolean handle(String input) {
                address = input;
                return true;
            }
        }, new SetupInput("Which port do you want to use") {
            @Override
            public List<String> getSuggestions() {
                return Collections.singletonList("3306");
            }

            @Override
            public boolean handle(String input) {
                port = input;
                return false;
            }
        }, new SetupInput("Which database do you want to use") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                database = input;
                return false;
            }
        }, new SetupInput("Which username do you want to use") {
            @Override
            public List<String> getSuggestions() {
                return Collections.singletonList("root");
            }

            @Override
            public boolean handle(String input) {
                username = input;
                return true;
            }
        }, new SetupInput("Which password do you want to use") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                password = input;
                return true;
            }
        });
    }
}
