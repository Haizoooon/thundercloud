package de.thundercloud.base.manager.template;

/*

  » de.thundercloud.base.manager.template

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 17:35

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.template.ITemplate;
import de.thundercloud.api.template.ITemplateManager;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.template.database.TemplateSqlAdapter;

import java.util.List;
import java.util.UUID;

public class TemplateManager extends TemplateSqlAdapter implements ITemplateManager {

    private final List<ITemplate> templates = Lists.newArrayList();

    public TemplateManager() {
        this.templates.addAll(getAllTemplates());
    }

    @Override
    public List<ITemplate> getTemplates() {
        return templates;
    }

    @Override
    public void createTemplate(String name, String staticType) {
        createRawTemplate(name, staticType);
        CloudBase.getBase().getFileHandler().createFolder("template/" + name);
    }

    @Override
    public boolean isTemplateExisting(String name) {
        return templateRawExisting(name);
    }

    @Override
    public boolean isTemplateExisting(UUID uuid) {
        return templateRawExisting(uuid);
    }
}
