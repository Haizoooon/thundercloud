package de.thundercloud.base.manager.database;

/*

  » de.thundercloud.base.manager.database

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:59

 */

import de.thundercloud.base.manager.database.security.SqlSecurityJsonObject;
import de.thundercloud.launcher.CloudBootstrap;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SqlConnector {

    private Connection connection;

    public void connect(SqlSecurityJsonObject sqlSecurityJsonObject) {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            connection = DriverManager.getConnection("jdbc:mysql://" + sqlSecurityJsonObject.getHostname() + ":3306/" +
                            sqlSecurityJsonObject.getDatabase() + "?useJDBCCompliantTimezoneShift=true&&serverTimezone=UTC&&useUnicode=true&autoReconnect=true",
                    sqlSecurityJsonObject.getUsername(), sqlSecurityJsonObject.getPassword());
        }catch (Exception ex){
            ex.printStackTrace();
        }
        CloudBootstrap.getBootstrap().getLoggerProvider().info("Successfully connected to mysql server.");
    }

    public void disconnect() {
        try {
            this.connection.close();
        } catch (SQLException throwable) {
            throwable.printStackTrace();
        }
    }

    public Connection getConnection() {
        return connection;
    }

}
