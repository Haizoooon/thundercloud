package de.thundercloud.base.manager.template;

/*

  » de.thundercloud.base.manager.template

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 17:34

 */

import de.thundercloud.api.template.ITemplate;
import de.thundercloud.api.template.TemplateType;

import java.util.UUID;

public class Template implements ITemplate {

    private final String name;
    private final UUID uuid;
    private final String staticType;

    public Template(String name, UUID uuid, String staticType) {
        this.name = name;
        this.uuid = uuid;
        this.staticType = staticType;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public UUID getId() {
        return uuid;
    }

    @Override
    public TemplateType getType() {
        return TemplateType.valueOf(staticType.toUpperCase());
    }
}
