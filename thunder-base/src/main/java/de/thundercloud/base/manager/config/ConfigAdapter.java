/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.manager.config;

/*

  » de.thundercloud.base.manager.config

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 11:19

 */

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ConfigAdapter {

    private JSONObject jsonObject;

    public ConfigAdapter(){
        try {
            String content = new String(Files.readAllBytes(Paths.get(new File("storage", "config.json").toURI())), StandardCharsets.UTF_8);
            jsonObject = new JSONObject(content);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void reload(){
        new ConfigAdapter();
    }

    public String getString(String content){
        return jsonObject.getString(content);
    }

    public List<Object> getList(String content){
        return jsonObject.getJSONArray(content).toList();
    }

}
