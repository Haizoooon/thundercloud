package de.thundercloud.base.manager.database.adapter;

/*

  » de.thundercloud.base.manager.database

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:50

 */

import de.thundercloud.base.manager.database.executor.ISqlExecutor;
import de.thundercloud.base.manager.database.types.SqlDataType;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public interface ISqlAdapter {

    void createTable(String tableName, Map<String, SqlDataType> content);

    List<Object> getListFromTable(String tableName, String column);

    Object getFromTable(String tableName, String column, String value, String neededColumn);
    Blob getBlobFromTable(String tableName, String column, String value, String neededColumn);

    ResultSet getResultsAllFromTable(String tableName, String column, String value);

    void removeMoreFromTable(String table, List<String> keys, List<String> values);

    void removeFromTable(String table, String column, String key);

    void removeAllFromTable(String table, String column, String value);

    boolean existsInTable(String table, String key, Object value);

    boolean existsMoreInTable(String table, String key, String value, String k2, String v2);

    void updateInTable(String table, String keyRow, String keyValue, String setRow, Object setValue);

    void updateAllInTable(String table, String setRow, String setValue);

    void addMoreInTable(final String table, List<String> keys, List<Object> values);

    int getTablePosition(String table, String type, String key, String searchBY);

    Map<Integer, String> sortByObject(String table, String key, String type, int max);

    Map<String, SqlDataType> getTableInformation(String[] keys, SqlDataType[] types);

    ISqlExecutor getSqlBaseExecutor();

}
