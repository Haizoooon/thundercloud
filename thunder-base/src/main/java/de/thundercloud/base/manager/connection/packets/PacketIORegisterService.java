package de.thundercloud.base.manager.connection.packets;

import de.thundercloud.api.packet.IPacket;
import de.thundercloud.api.service.ICloudService;

public class PacketIORegisterService extends IPacket {

    public PacketIORegisterService() {
        super("packetIORegisterService");
    }

    @Override
    public void read(ICloudService cloudService, String value) {

    }

    @Override
    public void write(ICloudService cloudService, String value) {

    }
}
