package de.thundercloud.base.manager.database.function;

/*

  » de.thundercloud.base.manager.database.function

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:46

 */

import java.sql.SQLException;

@FunctionalInterface
public interface ISqlFunction<I, O> {

    O apply(I i) throws SQLException;

}
