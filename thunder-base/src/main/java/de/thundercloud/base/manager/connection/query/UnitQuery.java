package de.thundercloud.base.manager.connection.query;

/*

  » de.thundercloud.base.manager.connection.query

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 11:27

 */

import de.thundercloud.api.unit.IUnitQuery;
import de.thundercloud.base.CloudBase;

public class UnitQuery implements IUnitQuery {

    @Override
    public void sendToAllTCP(String message) {
        CloudBase.getBase().getCloudConnectorServer().sendUnitQuery(message);
    }

    @Override
    public void sendQueryFromClient(String message) {

    }

}
