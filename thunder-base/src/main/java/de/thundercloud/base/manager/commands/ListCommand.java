package de.thundercloud.base.manager.commands;

/*

  » de.thundercloud.base.manager.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 01.04.2021 / 15:21

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.Color;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.base.CloudBase;
import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.launcher.console.logging.LoggerProvider;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "list", type = CommandType.CONSOLE)
public class ListCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender commandSender, String[] args) {
        LoggerProvider loggerProvider = CloudBootstrap.getBootstrap().getLoggerProvider();
        switch (args[1]) {
            case "service":
                loggerProvider.info("This is a list of all services...");
                loggerProvider.info(" ");
                CloudBase.getBase().getCloudServiceManager().getCloudServices().forEach(cloudService -> {
                    loggerProvider.info("§a" + cloudService.getServiceIdName());
                });
                loggerProvider.info(" ");
                break;

            case "group":
                loggerProvider.info("This is a list of all groups...");
                loggerProvider.info(" ");
                CloudBase.getBase().getCloudGroupServiceManager().getCloudGroupServiceList().forEach(cloudGroupService -> {
                    loggerProvider.info("§a" + cloudGroupService.getName());
                });
                loggerProvider.info(" ");
                break;
        }
    }

    @Override
    public String[] getUsage() {
        return new String[]{"list service » Lists all services", "list group » Lists all groups"};
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }
}
