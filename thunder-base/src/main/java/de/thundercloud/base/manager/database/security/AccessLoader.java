package de.thundercloud.base.manager.database.security;

import com.google.gson.Gson;

import java.io.*;

public class AccessLoader {

    private final Gson gson = new Gson();

    public SqlSecurityJsonObject loadObject() {
        SqlSecurityJsonObject object = null;
        try {
            object = gson.fromJson(new FileReader(getFile()), SqlSecurityJsonObject.class);
        } catch (FileNotFoundException exception) {
            exception.printStackTrace();
        }
        return object;
    }

    public void createSqlAccess(String host, int port, String database, String username, String password) {
        try (FileWriter fileWriter = new FileWriter(getFile())) {
            fileWriter.write(gson.toJson(new SqlSecurityJsonObject(host, username,
                    password, database, port)));

        } catch (IOException exception) {
            exception.printStackTrace();
        }

    }

    public boolean isAccessJsonExist() {
        return getFile().exists();
    }

    private File getFile() {
        return new File("sql.json");
    }
}
