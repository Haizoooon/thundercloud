/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.manager.commands;

/*

  » de.thundercloud.base.manager.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 12:03

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.api.console.LogType;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.base.CloudBase;
import org.jline.reader.Candidate;
import org.json.JSONObject;

import java.util.List;

@Command(name = "reload", type = CommandType.CONSOLE)
public class ReloadCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender iCommandSender, String[] args) {
        iCommandSender.sendMessage(LogType.INFO, "Starting reloading cloud...");

        CloudBase.getBase().getConfigAdapter().reload();
        for(ICloudService cloudService : CloudBase.getBase().getCloudServiceManager().getCloudServices()){
            if(cloudService.getGroupType().equals(GroupType.PROXY)){
                List<Object> list = CloudBase.getBase().getConfigAdapter().getList("tablist");
                List<Object> motd = CloudBase.getBase().getConfigAdapter().getList("motd");

                JSONObject tablistObject = new JSONObject();
                tablistObject.put("header", list.get(0));
                tablistObject.put("footer", list.get(1));

                JSONObject motdObject = new JSONObject();
                motdObject.put("line-1", motd.get(0));
                motdObject.put("line-2", motd.get(1));

                StringRequest tablistRequest = new StringRequest();
                tablistRequest.value = ConnectionProcess.CLOUDSERVICE_SEND_TABLIST + ";" + tablistObject.toString();

                cloudService.getConnection().sendTCP(tablistRequest);

                StringRequest motdRequest = new StringRequest();
                motdRequest.value = ConnectionProcess.CLOUDSERVICE_SEND_MOTD + ";" + motdObject.toString();

                cloudService.getConnection().sendTCP(motdRequest);
            }
        }

        iCommandSender.sendMessage(LogType.INFO, "Cloud was successfully reloaded!");
    }

    @Override
    public String[] getUsage() {
        return new String[0];
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }
}
