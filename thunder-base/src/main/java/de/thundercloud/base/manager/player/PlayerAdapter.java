/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.manager.player;

/*

  » de.thundercloud.base.manager.player

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 21:31

 */

import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.database.SqlAdapter;
import de.thundercloud.base.manager.database.types.SqlDataType;
import de.thundercloud.launcher.CloudBootstrap;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.UUID;

public class PlayerAdapter {

    private final String table = "cloud_players";
    private final String[] keys = new String[]{"uniqueId", "name", "info"};
    private final SqlDataType[] sqlDataTypes = new SqlDataType[]{SqlDataType.VARCHAR, SqlDataType.VARCHAR, SqlDataType.LONGBLOB};
    private final SqlAdapter sqlAdapter;

    public PlayerAdapter() {
        this.sqlAdapter = CloudBase.getBase().getDatabaseHandler().getSqlAdapter();
        createTable();
    }

    public void createTable(){
        sqlAdapter.createTable(table, sqlAdapter.getTableInformation(keys, sqlDataTypes));
    }

    public void createPlayer(UUID uuid, String name, String address, String port){
        if (!playerExists(uuid)) {

            JSONObject jsonObject = new JSONObject();

            JSONArray jsonArray = new JSONArray();
            jsonArray.put("default");

            jsonObject.put("groups", jsonArray);

            JSONObject addressObject = new JSONObject();
            addressObject.put("address", address);
            addressObject.put("port", port);

            jsonObject.put("connection", addressObject);

            sqlAdapter.addMoreInTable(table, Arrays.asList(keys), Arrays.asList(uuid.toString(), name, jsonObject.toString()));

        }
    }

    public String getJsonString(String name){
        return String.valueOf(sqlAdapter.getFromTable(table, keys[1], name, keys[2]));
    }

    public boolean playerExists(UUID uuid){
        return sqlAdapter.existsInTable(table, keys[0], uuid.toString());
    }

}
