package de.thundercloud.base.manager.database;

/*

  » de.thundercloud.base.manager.database

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:48

 */

import de.thundercloud.base.manager.database.executor.ISqlExecutor;
import de.thundercloud.base.manager.database.function.ISqlFunction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SqlExecutor implements ISqlExecutor {

    private final Connection connection;

    public SqlExecutor(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T> T executeQuery(String query, ISqlFunction<ResultSet, T> function, T defaultValue) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)){
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                return function.apply(resultSet);
            } catch (Exception exception){
                return defaultValue;
            }
        } catch (SQLException exception){
            exception.printStackTrace();;
        }
        return null;
    }

    @Override
    public int executeUpdate(String query) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)){
            return preparedStatement.executeUpdate();
        } catch (SQLException exception){
            exception.printStackTrace();
            return -1;
        }
    }

}
