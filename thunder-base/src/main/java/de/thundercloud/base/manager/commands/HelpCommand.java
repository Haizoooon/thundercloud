package de.thundercloud.base.manager.commands;

/*

  » de.thundercloud.base.manager.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 01.04.2021 / 10:05

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.launcher.CloudBootstrap;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "help", type = CommandType.CONSOLE)
public class HelpCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender commandSender, String[] args) {
        if(args[0].equalsIgnoreCase("help")){
            CloudBootstrap.getBootstrap().getCommandManager().getCommandHandlers().forEach((key, value) -> {
                for (String string : value.getUsage()) {
                    CloudBootstrap.getBootstrap().getLoggerProvider().info(string);
                }
            });
        }
    }

    @Override
    public String[] getUsage() {
        return new String[]{"help » Shows this message"};
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }
}
