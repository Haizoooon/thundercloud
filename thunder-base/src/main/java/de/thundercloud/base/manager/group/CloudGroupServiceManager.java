package de.thundercloud.base.manager.group;

/*

  » de.thundercloud.base.manager.group

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 13:25

 */

import com.google.common.collect.Lists;
import de.thundercloud.base.manager.group.database.CloudGroupSqlAdapter;
import de.thundercloud.launcher.CloudBootstrap;

import java.util.List;

public class CloudGroupServiceManager extends CloudGroupSqlAdapter {

    private final List<CloudGroupService> cloudGroupServiceList = Lists.newArrayList();

    public CloudGroupServiceManager() {
        cloudGroupServiceList.addAll(getAllGroupServices());
        CloudBootstrap.getBootstrap().getLoggerProvider().info("Loaded following groups:");
        cloudGroupServiceList.forEach(cloudGroupService -> CloudBootstrap.getBootstrap().getLoggerProvider().info("- " + cloudGroupService.getName()));
    }

    public List<CloudGroupService> getCloudGroupServiceList() {
        return cloudGroupServiceList;
    }
}
