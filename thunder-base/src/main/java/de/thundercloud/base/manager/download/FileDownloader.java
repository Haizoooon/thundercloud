/*
 * » Created by Niklas Sch. on 12.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.manager.download;

/*

  » de.thundercloud.base.manager.download

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 12.04.2021 / 13:01

 */

import de.thundercloud.api.group.types.GroupVersion;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class FileDownloader {

    private GroupVersion groupVersion;

    public FileDownloader(GroupVersion groupVersion){
        this.groupVersion = groupVersion;
        new File("storage/jars").mkdirs();
    }

    public void download(){
        try {

            URL url = new URL(groupVersion.getLink());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0");
            BufferedInputStream bufferedInputStream = new BufferedInputStream(connection.getInputStream());
            FileOutputStream fileOutputStream = new FileOutputStream(new File("storage/jars", groupVersion.getDisplay() + ".jar"));
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream, 1024);
            byte[] buffer = new byte[1024];
            int read = 0;
            while((read  = bufferedInputStream.read(buffer, 0, 1024)) >= 0){
                bufferedOutputStream.write(buffer, 0, read);
            }

            bufferedOutputStream.close();
            bufferedInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
