package de.thundercloud.base.manager.commands;

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.base.manager.setups.GroupSetup;
import de.thundercloud.base.manager.setups.TemplateSetup;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "create", type = CommandType.CONSOLE)
public class CreateCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender iCommandSender, String[] args) {
        if(args[0].equalsIgnoreCase("create")){
            switch (args[1]){
                case "template":
                    new TemplateSetup();
                    break;

                case "group":
                    new GroupSetup();
                    break;
            }
        }
    }

    @Override
    public String[] getUsage() {
        return new String[]{"create template » Creates a template (Setup)", "create group » Creates a group (Setup)"};
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }
}
