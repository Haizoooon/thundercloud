package de.thundercloud.base.manager.database;

/*

  » de.thundercloud.base.manager.database

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 05.04.2021 / 17:54

 */

import com.google.common.collect.Lists;
import de.thundercloud.base.manager.database.adapter.ISqlAdapter;
import de.thundercloud.base.manager.database.executor.ISqlExecutor;
import de.thundercloud.base.manager.database.types.SqlDataType;

import java.sql.Blob;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

public class SqlAdapter implements ISqlAdapter {

    private final ISqlExecutor sqlBaseExecutor;

    public SqlAdapter(ISqlExecutor sqlBaseExecutor) {
        this.sqlBaseExecutor = sqlBaseExecutor;
    }

    @Override
    public void createTable(String tableName, Map<String, SqlDataType> content) {
        StringBuilder stringBuilder = new StringBuilder("CREATE TABLE IF NOT EXISTS ").append(tableName).append(" (");
        int count = 0;
        for(String key : content.keySet()){
            stringBuilder.append(key).append(" ").append(content.get(key).getSqlTag()).append(count + 1 >= content.size() ? ")" : ", ");
            count++;
        }
        CompletableFuture.supplyAsync(() -> sqlBaseExecutor.executeUpdate(stringBuilder.toString()));
    }

    @Override
    public List<Object> getListFromTable(String tableName, String column) {
        List<Object> list = this.sqlBaseExecutor.executeQuery("SELECT * FROM " + tableName, resultSet -> {
            List<Object> content = Lists.newArrayList();
            while (resultSet.next()) content.add(resultSet.getString(column));
            return content;
        }, Lists.newArrayList());
        return !list.isEmpty() ? list : null;
    }

    @Override
    public void removeFromTable(String table, String column, String key) {
        this.sqlBaseExecutor.executeUpdate("DELETE FROM " + table + " WHERE " + column + "='" + key + "'");
    }

    @Override
    public boolean existsInTable(String table, String key, Object value) {
        return this.sqlBaseExecutor.executeQuery(
                "SELECT * FROM " + table + " WHERE " + key + "='" + value + "'", ResultSet::next, false
        );
    }

    @Override
    public void updateInTable(String table, String keyRow, String keyValue, String setRow, Object setValue) {
        CompletableFuture.supplyAsync(() -> this.sqlBaseExecutor.executeUpdate("UPDATE " + table +
                " SET " + setRow + "= '" + setValue + "' WHERE " + keyRow + "= '" + keyValue + "';"));
    }

    @Override
    public void addMoreInTable(String table, List<String> types, List<Object> list) {
        StringBuilder upload = new StringBuilder("INSERT INTO " + table + "(" + types.get(0));
        for (int i = 1; i < types.size(); i++) upload.append(", ").append(types.get(i));
        upload.append(") VALUES ('").append(list.get(0)).append("'");
        for (int i = 1; i < list.size(); i++) upload.append(", '").append(list.get(i)).append("'");
        upload.append(");");
        CompletableFuture.supplyAsync(() -> this.sqlBaseExecutor.executeUpdate(upload.toString()));
    }

    @Override
    public Object getFromTable(String tableName, String column, String value, String neededColumn) {
        return sqlBaseExecutor.executeQuery("SELECT * FROM " + tableName + " WHERE " + column + "='" + value + "'", resultSet -> {
            if (resultSet.next()) {
                return resultSet.getString(neededColumn);
            }
            return null;
        }, "null");
    }

    @Override
    public Blob getBlobFromTable(String tableName, String column, String value, String neededColumn) {
        return sqlBaseExecutor.executeQuery("SELECT * FROM " + tableName + " WHERE " + column + "='" + value + "'", resultSet -> {
            if (resultSet.next()) {
                return resultSet.getBlob(neededColumn);
            }
            return null;
        }, null);
    }

    @Override
    public ResultSet getResultsAllFromTable(String tableName, String column, String value) {
        return this.sqlBaseExecutor.executeQuery("SELECT * FROM " + tableName + " WHERE " + column + "='" + value + "'",
                resultSet -> resultSet, null);
    }

    @Override
    public void removeMoreFromTable(String table, List<String> keys, List<String> values) {
        StringBuilder queryBuilder = new StringBuilder().append("DELETE FROM ").append(table).append(" WHERE ");
        for (int i = 0; i < keys.size(); ++i)
            queryBuilder.append(keys.get(i)).append("='").append(values.get(i)).append("'").append(i + 1 >= keys.size() ? "" : " AND ");
        CompletableFuture.supplyAsync(() -> this.sqlBaseExecutor.executeUpdate(queryBuilder.toString()));
    }

    @Override
    public void removeAllFromTable(String table, String column, String value) {
        CompletableFuture.supplyAsync(() -> this.sqlBaseExecutor.executeUpdate("DELETE FROM " + table + " WHERE " + column + "='" + value + "'"));
    }

    @Override
    public boolean existsMoreInTable(String table, String key, String value, String k2, String v2) {
        return this.sqlBaseExecutor.executeQuery("SELECT * FROM " + table + " WHERE " + key + "='" + value + "' AND " +
                k2 + "='" + v2 + "'", ResultSet::next, false);
    }

    @Override
    public void updateAllInTable(String table, String setRow, String setValue) {
        CompletableFuture.supplyAsync(() ->this.sqlBaseExecutor.executeUpdate("UPDATE " + table + " SET " + setRow + "='" + setValue + "'"));
    }

    @Override
    public int getTablePosition(String table, String type, String key, String searchBY) {
        AtomicInteger count = new AtomicInteger();
        Number place = this.sqlBaseExecutor.executeQuery("SELECT * FROM " + table + " ORDER BY " + searchBY + " DESC", resultSet -> {
            while (resultSet.next()) {
                if (resultSet.getString(type).equalsIgnoreCase(key)) {
                    count.set(resultSet.getRow());
                    return count;
                }
            }
            return -1;
        }, -1);
        return place.intValue();
    }

    @Override
    public Map<Integer, String> sortByObject(String table, String key, String type, int max) {
        AtomicInteger count = new AtomicInteger(1);
        Map<Integer, String> list = new HashMap<>();
        this.sqlBaseExecutor.executeQuery("SELECT * FROM " + table + " ORDER BY " + type + " DESC LIMIT " + max, resultSet -> {
            while (resultSet.next()) list.put(count.getAndIncrement(), resultSet.getString(key));
            return list;
        }, list);
        return list;

    }

    @Override
    public Map<String, SqlDataType> getTableInformation(String[] keys, SqlDataType[] types){
        Map<String, SqlDataType> content = new HashMap<>();
        for(int i = 0; i < keys.length; i++) content.put(keys[i], types[i]);
        return content;
    }

    @Override
    public ISqlExecutor getSqlBaseExecutor() {
        return sqlBaseExecutor;
    }
}
