package de.thundercloud.base.manager.service;

/*

  » de.thundercloud.base.manager.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 01.04.2021 / 14:48

 */

import com.esotericsoftware.kryonet.Connection;
import com.google.common.collect.Lists;

import de.thundercloud.api.group.ICloudGroupService;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.api.service.process.ICloudServiceProcessManager;
import de.thundercloud.api.service.state.ServiceState;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.group.CloudGroupService;
import de.thundercloud.launcher.files.FileHandler;
import de.thundercloud.launcher.files.JsonLib;
import de.thundercloud.base.wrapper.process.CloudProcessManager;
import de.thundercloud.launcher.CloudBootstrap;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class CloudService implements ICloudService {

    private final String name;
    private final int serviceId;
    private final int port;
    private final GroupType groupType;
    private final GroupVersion groupVersion;
    private Connection connection;
    private ServiceState serviceState;
    private int onlinePlayers;
    private final CloudGroupService cloudGroupService;

    private final CopyOnWriteArrayList<String> cachedScreenMessages;
    private boolean authenticated;
    private final CloudProcessManager cloudProcessManager;

    public CloudService(String name, int serviceId, GroupType groupType, GroupVersion groupVersion) {
        this.name = name;
        this.serviceId = serviceId;
        this.groupType = groupType;
        this.groupVersion = groupVersion;
        this.port = randomPort();
        this.serviceState = ServiceState.PREPARING;
        this.cachedScreenMessages = new CopyOnWriteArrayList<>();
        this.authenticated = false;
        this.onlinePlayers = 0;
        this.cloudProcessManager = new CloudProcessManager(this);
        this.cloudGroupService = CloudBase.getBase().getCloudGroupServiceManager().getCloudGroupServiceList().stream().filter(service -> service.getName().equalsIgnoreCase(name)).findAny().orElse(null);
    }

    public void start() {
        CloudBootstrap.getBootstrap().getLoggerProvider().info("Cloud service §a" + getServiceIdName() + " §ris starting");

        FileHandler fileHandler = CloudBase.getBase().getFileHandler();

        if (fileHandler.fileExist("template/" + getName(), "THUNDERCLOUD.json")) {
            fileHandler.deleteFile("template/" + getName(), "THUNDERCLOUD.json");
        }

        switch (getGroupVersion()) {
            case BUNGEECORD:
                fileHandler.deleteFile("template/" + getName(), "config.yml");
                break;

            case WATERFALL:
                fileHandler.deleteFile("template/" + getName(), "config.yml");
                fileHandler.deleteFile("template/" + getName(), "waterfall.yml");
                break;

            case VELOCITY:
                fileHandler.deleteFile("template/" + getName(), "velocity.toml");
                break;

            default:
                fileHandler.deleteFile("template/" + getName(), "server.properties");
                fileHandler.deleteFile("template/" + getName(), "bukkit.yml");
                fileHandler.deleteFile("template/" + getName(), "spigot.yml");
                break;
        }


        switch (cloudGroupService.getTemplate().getType()) {

            case DYNAMIC:
            case STATIC:

                if (fileHandler.folderExist("temp/" + getServiceIdName()))
                    fileHandler.deleteFiles(new File("temp/" + getServiceIdName()));

                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                fileHandler.createFolder("temp/" + getServiceIdName() + "/plugins");

                fileHandler.copyFile("storage/jars/" + getGroupVersion().getDisplay() + ".jar", "temp/" + getServiceIdName() + "/" + getGroupVersion().getDisplay() + ".jar");

                fileHandler.copyFile("storage/jars/thunder-plugin-1.0-SNAPSHOT-shaded.jar", "temp/" + getServiceIdName() + "/plugins/thunder-plugin-1.0-SNAPSHOT-shaded.jar");

                fileHandler.createFile("temp/" + getServiceIdName(), "THUNDERCLOUD.json");

                CloudBase.getBase().getModuleHandler().getModules().forEach(thunderModule -> {
                    fileHandler.copyFile("modules/" + thunderModule.getName() + ".jar", "temp/" + getServiceIdName() + "/plugins/" + thunderModule.getName() + ".jar");
                });

                break;

            case TEMPLATE:

                fileHandler.createFolder("temp/" + getServiceIdName());

                fileHandler.copyDirectory("template/" + getName(), "temp/" + getServiceIdName());

                fileHandler.createFolder("temp/" + getServiceIdName() + "/plugins");

                if (fileHandler.fileExist("temp/" + getServiceIdName(), getGroupVersion().getDisplay() + ".jar")) {
                    fileHandler.deleteFile("temp/" + getServiceIdName(), getGroupVersion().getDisplay() + ".jar");
                }

                if (!fileHandler.fileExist("temp/" + getServiceIdName(), getGroupVersion().getDisplay() + ".jar")) {
                    fileHandler.copyFile("storage/jars/" + getGroupVersion().getDisplay() + ".jar", "temp/" + getServiceIdName() + "/" + getGroupVersion().getDisplay() + ".jar");
                }

                if (fileHandler.fileExist("temp/" + getServiceIdName(), "thunder-plugin-1.0-SNAPSHOT-shaded.jar")) {
                    fileHandler.deleteFile("temp/" + getServiceIdName(), "thunder-plugin-1.0-SNAPSHOT-shaded.jar");
                }

                if (!fileHandler.fileExist("temp/" + getServiceIdName(), "thunder-plugin-1.0-SNAPSHOT-shaded.jar")) {
                    fileHandler.copyFile("storage/jars/thunder-plugin-1.0-SNAPSHOT-shaded.jar", "temp/" + getServiceIdName() + "/plugins/thunder-plugin-1.0-SNAPSHOT-shaded.jar");
                }

                CloudBase.getBase().getModuleHandler().getModules().forEach(thunderModule -> {
                    if (fileHandler.fileExist("temp/" + getServiceIdName(), thunderModule.getName() + ".jar")) {
                        fileHandler.deleteFile("temp/" + getServiceIdName(), thunderModule.getName() + ".jar");
                    }
                    if (!fileHandler.fileExist("temp/" + getServiceIdName(), thunderModule.getName() + ".jar")) {
                        fileHandler.copyFile("modules/" + thunderModule.getName() + ".jar", "temp/" + getServiceIdName() + "/plugins/" + thunderModule.getName() + ".jar");
                    }
                });

                break;

        }

        try {
            FileWriter fileWriter = new FileWriter(new File("temp/" + getServiceIdName(), "THUNDERCLOUD.json"));

            fileWriter.write(new JsonLib()
                    .append("serviceName", getName())
                    .append("serviceId", getServiceId())
                    .append("groupType", getGroupType().getDisplay())
                    .append("groupVersion", getGroupVersion().getDisplay()).appendAll());

            fileWriter.close();

        } catch (
                IOException e) {
            e.printStackTrace();
        }

        cloudProcessManager.start();

    }

    public void stop() {
        if (connection != null) {

            switch (getGroupVersion()) {
                case BUNGEECORD:
                case WATERFALL:
                    cloudProcessManager.executeCommand("end");
                    break;
                case VELOCITY:

                    break;

                default:
                    cloudProcessManager.executeCommand("stop");
            }

            cloudProcessManager.stop();

            CloudBootstrap.getBootstrap().getLoggerProvider().info("Cloud service §c" + getServiceIdName() + " §rwas stopped!");

        }
    }

    private int randomPort() {
        int port = new Random().nextInt(50000);
        if (port > 10000) {
            return port;
        } else randomPort();
        return port;
    }

    public String getServiceStateString() {
        switch (serviceState) {
            case PREPARING:
                return "§e" + serviceState.getDisplay();
            case STARTING:
                return "§9" + serviceState.getDisplay();
            case STARTED:
                return "§a" + serviceState.getDisplay();
            case CLOSED:
                return "§c" + serviceState.getDisplay();
            default:
                return " ";
        }
    }

    @Override
    public ICloudServiceProcessManager getServiceProcessManager() {
        return cloudProcessManager;
    }

    @Override
    public void setServiceState(ServiceState serviceState) {
        this.serviceState = serviceState;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getServiceIdName() {
        return name + "-" + serviceId;
    }

    @Override
    public int getServiceId() {
        return serviceId;
    }

    @Override
    public int getPort() {
        return port;
    }

    @Override
    public ICloudGroupService getGroupService() {
        return cloudGroupService;
    }

    @Override
    public GroupVersion getGroupVersion() {
        return groupVersion;
    }

    @Override
    public GroupType getGroupType() {
        return groupType;
    }

    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    @Override
    public CopyOnWriteArrayList<String> getCachedScreenMessages() {
        return cachedScreenMessages;
    }

    public ServiceState getServiceState() {
        return serviceState;
    }

    @Override
    public boolean isAuthenticated() {
        return authenticated;
    }

    @Override
    public void setAuthenticated(boolean value) {
        this.authenticated = value;
    }

    @Override
    public void setOnlinePlayers(int value) {
        this.onlinePlayers = value;
    }

    @Override
    public int getOnlinePlayers() {
        return onlinePlayers;
    }
}
