package de.thundercloud.base.manager.template.database;

/*

  » de.thundercloud.base.manager.template.database

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 17:35

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.template.ITemplate;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.database.SqlAdapter;
import de.thundercloud.base.manager.database.types.SqlDataType;
import de.thundercloud.base.manager.template.Template;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public abstract class TemplateSqlAdapter {

    private String table = "cloud_templates";
    private String[] keys = new String[]{"name", "id", "staticType"};
    private SqlDataType[] sqlDataTypes = new SqlDataType[]{SqlDataType.VARCHAR, SqlDataType.VARCHAR , SqlDataType.VARCHAR};
    private SqlAdapter sqlAdapter;

    public TemplateSqlAdapter(){
        this.sqlAdapter = CloudBase.getBase().getDatabaseHandler().getSqlAdapter();
        createTable();
    }

    public void createTable(){
        this.sqlAdapter.createTable(table, sqlAdapter.getTableInformation(keys, sqlDataTypes));
    }

    public boolean templateRawExisting(String name){
        return sqlAdapter.existsInTable(table, keys[0], name);
    }

    public boolean templateRawExisting(UUID uuid){
        return sqlAdapter.existsInTable(table, keys[1], uuid.toString());
    }

    public void createRawTemplate(String name, String staticType){
        UUID uuid = UUID.randomUUID();
        if (!templateRawExisting(name))
            sqlAdapter.addMoreInTable(table, Arrays.asList(keys), Arrays.asList(name, uuid.toString(), staticType));
    }

    public List<ITemplate> getAllTemplates(){
        List<ITemplate> templates = sqlAdapter.getSqlBaseExecutor().executeQuery("SELECT * FROM " + table, resultSet -> {
            List<ITemplate> content = Lists.newArrayList();
            while(resultSet.next()){
                String name = resultSet.getString("name");
                UUID uuid = UUID.fromString(resultSet.getString("id"));
                String staticType = resultSet.getString("staticType");
                content.add(new Template(name, uuid, staticType));
            }
            return content;
        }, Lists.newArrayList());
        return !templates.isEmpty() ? templates : Lists.newArrayList();
    }

}
