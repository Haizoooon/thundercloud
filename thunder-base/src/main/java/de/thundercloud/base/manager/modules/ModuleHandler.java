package de.thundercloud.base.manager.modules;

/*

  » de.thundercloud.base.manager.modules

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 20:20

 */

import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.launcher.external.reader.JsonReader;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

public class ModuleHandler {

    private final ModuleInitializer moduleInitializer;
    public List<ThunderModule> modules;

    public ModuleHandler() {
        this.modules = new ArrayList<>();
        moduleInitializer = new ModuleInitializer();
    }

    private File getDirectory(){
        return new File("modules");
    }

    public void registerModules(){
        File[] files = getDirectory().listFiles();
        if(files == null){
            return;
        }

        JarFile jarFile = null;

        for(File file : Arrays.stream(files).filter(Objects::nonNull).filter(this::isJarFile).collect(Collectors.toList())){
            try {

                jarFile = new JarFile(file);
                JarEntry jarEntry = jarFile.getJarEntry("thunder-module.json");

                JsonReader jsonReader;

                try (InputStream inputStream = jarFile.getInputStream(jarEntry)){
                    String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
                    jsonReader = new JsonReader(content);
                }

                moduleInitializer.initModule(file.getName(), new ThunderModuleDescription(jsonReader.read("name"), jsonReader.read("main")));

                List<String> maps = new ArrayList<>();
                maps.add("HAHA");

                String map = maps.get(new Random().nextInt(maps.size()));

            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            } finally {
                try {
                    assert jarFile != null;
                    jarFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void unregisterModules(){
        modules.forEach(module -> {
            CloudBootstrap.getBootstrap().getLoggerProvider().info("Cloud module §a" + module.getName() + " §rwas unregistered!");
            modules.remove(module);
        });
    }


    public File findModule(String name) { return new File("modules", name); }

    public boolean isJarFile(File file){
        return file.getName().endsWith(".jar");
    }

    public List<ThunderModule> getModules() {
        return modules;
    }

}
