package de.thundercloud.base.manager.commands;

/*

  » de.thundercloud.base.manager.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 01.04.2021 / 15:39

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.Color;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.base.CloudBase;
import de.thundercloud.launcher.CloudBootstrap;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "stop", type = CommandType.CONSOLE)

public class StopCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender commandSender, String[] args) {
        if(args[0].equalsIgnoreCase("stop")){

            CloudBase.getBase().getCloudServiceManager().getCloudServices().forEach(iCloudService -> {
                CloudBootstrap.getBootstrap().getLoggerProvider().info("Cloud service §a" + iCloudService.getServiceIdName() + " §ris stopping");
                iCloudService.stop();
            });
            CloudBootstrap.getBootstrap().getLoggerProvider().info("Cloud was stopped");
            CloudBase.getBase().getDatabaseHandler().disconnect();
            CloudBase.getBase().getCloudConnectorServer().disconnect();
            CloudBootstrap.getBootstrap().getConsoleManager().stopThread();
            System.exit(1);

        }
    }

    @Override
    public String[] getUsage() {
        return new String[]{"stop » Stops the cloud"};
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }
}
