package de.thundercloud.base.manager.setups;

import com.google.common.collect.Lists;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.api.template.ITemplate;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.download.FileDownloader;
import de.thundercloud.base.manager.group.CloudGroupService;
import de.thundercloud.launcher.console.setup.SetupBuilder;
import de.thundercloud.launcher.console.setup.abstracts.SetupEnd;
import de.thundercloud.launcher.console.setup.abstracts.SetupInput;
import de.thundercloud.launcher.console.setup.interfaces.ISetup;

import java.util.Arrays;
import java.util.List;

public class GroupSetup implements ISetup {

    private String name, template, minServers, maxServers, maxMemory, maxPlayers, percentageToStart, groupVersion, groupType;

    public GroupSetup() {
        new SetupBuilder(this, new SetupEnd() {
            @Override
            public void handle() {

                GroupVersion version = Arrays.stream(GroupVersion.values()).filter(value -> value.getDisplay().equalsIgnoreCase(groupVersion)).findAny().orElse(null);
                GroupType type = Arrays.stream(GroupType.values()).filter(value -> value.getDisplay().equalsIgnoreCase(groupType)).findAny().orElse(null);
                int finalMinServers = Integer.parseInt(minServers);
                int finalMaxServers = Integer.parseInt(maxServers);
                int finalMaxMemory = Integer.parseInt(maxMemory);
                int finalMaxPlayers = Integer.parseInt(maxPlayers);
                int finalPercentage = Integer.parseInt(percentageToStart);

                assert type != null;
                assert version != null;

                CloudBase.getBase().getCloudGroupServiceManager().createGroup(name, finalMaxServers, finalMinServers, finalMaxMemory, finalPercentage, finalMaxPlayers, true, type, version);

                CloudGroupService cloudGroupService = new CloudGroupService(name, finalMaxServers, finalMinServers, finalMaxMemory, finalPercentage, finalMaxPlayers, false, type, version);

                if(!CloudBase.getBase().getFileHandler().fileExist("storage/jars", version.getDisplay() + ".jar")){
                    new FileDownloader(version).download();
                }

                CloudBase.getBase().getCloudGroupServiceManager().getCloudGroupServiceList().add(cloudGroupService);

                for(int i = 0; i < finalMinServers; i++){
                    CloudBase.getBase().getCloudServiceManager().addService(i + 1, cloudGroupService);
                }


            }
        }, new SetupInput("What name should the group have?") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                name = input;
                return true;
            }

        }, new SetupInput("Which template name?") {
            @Override
            public List<String> getSuggestions() {
                List<String> strings = Lists.newArrayList();
                for (ITemplate temp : CloudBase.getBase().getTemplateManager().getAllTemplates()) {
                    strings.add(temp.getName());
                }
                return strings;
            }

            @Override
            public boolean handle(String input) {
                template = input.replace(" ", "");
                return CloudBase.getBase().getTemplateManager().isTemplateExisting(template);
            }

        }, new SetupInput("How many people can there be in the group?") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                maxPlayers = input;
                return isInteger(input);
            }

        }, new SetupInput("How many servers should be online at least?") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                minServers = input;
                return isInteger(input);
            }

        }, new SetupInput("How many servers should be online at most?") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                maxServers = input;
                return isInteger(input);
            }

        }, new SetupInput("How much memory should the group have?") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                maxMemory = input;
                return isInteger(input);
            }

        }, new SetupInput("How many percent should the server be full before a new one starts?") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                percentageToStart = input;
                return isInteger(input);
            }

        }, new SetupInput("Which group type should the group have?") {
            @Override
            public List<String> getSuggestions() {
                List<String> types = Lists.newArrayList();
                for (GroupType type : GroupType.values()) {
                    types.add(type.getDisplay());
                }
                return types;
            }

            @Override
            public boolean handle(String input) {
                groupType = input.replace(" ", "");
                return getSuggestions().contains(groupType);
            }

        }, new SetupInput("Which Minecraft version do you want the group to have?") {
            @Override
            public List<String> getSuggestions() {
                List<String> versions = Lists.newArrayList();
                for (GroupVersion version : GroupVersion.values()) {
                    if(version.getGroupType().equals(GroupType.valueOf(groupType.toUpperCase()))){
                        versions.add(version.getDisplay());
                    }
                }
                return versions;
            }

            @Override
            public boolean handle(String input) {
                groupVersion = input.replace(" ", "");
                return getSuggestions().contains(groupVersion);
            }
        });
    }

    private boolean isInteger(String input) {
        return input.matches("[0-9]+");
    }

}