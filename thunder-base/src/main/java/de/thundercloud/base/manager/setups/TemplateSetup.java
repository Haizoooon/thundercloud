/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.manager.setups;

import de.thundercloud.base.CloudBase;
import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.launcher.console.setup.SetupBuilder;
import de.thundercloud.launcher.console.setup.abstracts.SetupEnd;
import de.thundercloud.launcher.console.setup.abstracts.SetupInput;
import de.thundercloud.launcher.console.setup.interfaces.ISetup;

import java.util.Arrays;
import java.util.List;

public class TemplateSetup implements ISetup {

    private String name;
    private String staticType;

    public TemplateSetup() {

        new SetupBuilder(this, new SetupEnd() {
            @Override
            public void handle() {

                CloudBase.getBase().getTemplateManager().createTemplate(name, staticType);
                CloudBootstrap.getBootstrap().getLoggerProvider().info("The template §a" + name + " §rwas created");

            }
        }, new SetupInput("How should the template be named (without space)") {
            @Override
            public List<String> getSuggestions() {
                return null;
            }

            @Override
            public boolean handle(String input) {
                name = input.replace(" ", "");
                return CloudBase.getBase().getTemplateManager().getTemplates().stream().noneMatch(iTemplate -> iTemplate.getName().equalsIgnoreCase(name));
            }
        }, new SetupInput("Which type do you want? (dynamic / template / static)") {
            @Override
            public List<String> getSuggestions() {
                return Arrays.asList("dynamic", "template", "static");
            }

            @Override
            public boolean handle(String input) {
                staticType = input.replace(" ", "");
                return getSuggestions().contains(staticType);
            }
        });
    }
}
