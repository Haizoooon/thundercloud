package de.thundercloud.base.manager.connection;

/*

  » de.thundercloud.base.manager.connection

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 06.04.2021 / 10:48

 */

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.api.response.StringResponse;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.api.service.state.ServiceState;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.service.CloudService;
import de.thundercloud.launcher.CloudBootstrap;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

public class CloudConnectorServer {

    private Server server;

    public CloudConnectorServer() {
        connect();
    }

    public void connect(){
        try {
            server = new Server();
            server.start();
            server.bind(40017, 40018);

            CloudBootstrap.getBootstrap().getLoggerProvider().info("Trying to connect... (Kryo)");
            CloudBootstrap.getBootstrap().getLoggerProvider().info("UDP: 40017");
            CloudBootstrap.getBootstrap().getLoggerProvider().info("TCP: 40018");

            Kryo kryo = server.getKryo();
            kryo.register(StringRequest.class);
            kryo.register(StringResponse.class);

            server.addListener(new Listener() {

                @Override
                public void received(Connection connection, Object o) {
                    if(o instanceof StringRequest){
                        StringRequest stringRequest = (StringRequest) o;

                        String[] args = stringRequest.value.split(";");

                        ConnectionProcess connectionProcess = Arrays.stream(ConnectionProcess.values()).filter(process -> process.name().equalsIgnoreCase(args[0])).findAny().orElse(null);

                        if(connectionProcess == null){
                            return;
                        }

                        if(connectionProcess == ConnectionProcess.PROXY_PLAYER_REGISTERED){

                            UUID uuid = UUID.fromString(args[1]);
                            String name = args[2];
                            String address = args[3];
                            String port = args[4];

                            CloudBase.getBase().getPlayerAdapter().createPlayer(uuid, name, address, port);

                        }

                        if(connectionProcess == ConnectionProcess.CLOUDSERVICE_COPY){

                            ICloudService cloudService = CloudBase.getBase().getCloudServiceManager().getCloudServices().stream().filter(service -> service.getServiceIdName().equalsIgnoreCase(args[1])).findAny().orElse(null);

                            if(cloudService == null){
                                CloudBootstrap.getBootstrap().getLoggerProvider().severe("Cloud service §a" + args[1] + " §rwas not found...");
                                return;
                            }

                            CloudBase.getBase().getFileHandler().deleteFiles(new File("template/" + cloudService.getName()));
                            CloudBase.getBase().getFileHandler().copyDirectory("temp/" + cloudService.getServiceIdName(), "template/" + cloudService.getName());

                            CloudBootstrap.getBootstrap().getLoggerProvider().severe("Cloud service §a" + args[1] + " §rwas copied.");

                        }

                        if(connectionProcess == ConnectionProcess.CLOUDSERVICE_REGISTERED){
                            ICloudService cloudService = CloudBase.getBase().getCloudServiceManager().getCloudServices().stream().filter(service -> service.getServiceIdName().equalsIgnoreCase(args[1])).findAny().orElse(null);

                            if(cloudService == null){
                                return;
                            }

                            cloudService.setConnection(connection);
                            cloudService.setServiceState(ServiceState.STARTED);

                            if (cloudService.getGroupType().equals(GroupType.PROXY)) {

                                List<Object> list = CloudBase.getBase().getConfigAdapter().getList("tablist");
                                List<Object> motd = CloudBase.getBase().getConfigAdapter().getList("motd");

                                JSONObject tablistObject = new JSONObject();
                                tablistObject.put("header", list.get(0));
                                tablistObject.put("footer", list.get(1));

                                JSONObject motdObject = new JSONObject();
                                motdObject.put("line-1", motd.get(0));
                                motdObject.put("line-2", motd.get(1));

                                StringRequest tablistRequest = new StringRequest();
                                tablistRequest.value = ConnectionProcess.CLOUDSERVICE_SEND_TABLIST + ";" + tablistObject.toString();

                                cloudService.getConnection().sendTCP(tablistRequest);

                                StringRequest motdRequest = new StringRequest();
                                motdRequest.value = ConnectionProcess.CLOUDSERVICE_SEND_MOTD + ";" + motdObject.toString();

                                cloudService.getConnection().sendTCP(motdRequest);

                                JSONObject messageObject = new JSONObject();
                                messageObject.put("prefix", CloudBase.getBase().getConfigAdapter().getString("prefix"));
                                messageObject.put("serviceStarted", CloudBase.getBase().getConfigAdapter().getString("serviceStarted"));
                                messageObject.put("serviceStopped", CloudBase.getBase().getConfigAdapter().getString("serviceStopped"));

                                StringRequest messageRequest = new StringRequest();
                                messageRequest.value = ConnectionProcess.CLOUD_SEND_MESSAGES + ";" + messageObject.toString();
                                cloudService.getConnection().sendTCP(messageRequest);

                            }

                            JSONObject cloudServiceImpl = new JSONObject();
                            cloudServiceImpl.put("serviceName", cloudService.getName());
                            cloudServiceImpl.put("serviceId", cloudService.getServiceId());
                            cloudServiceImpl.put("port", cloudService.getPort());
                            cloudServiceImpl.put("groupVersion", cloudService.getGroupVersion().getDisplay());
                            cloudServiceImpl.put("groupType", cloudService.getGroupType().getDisplay());
                            cloudServiceImpl.put("serviceState", cloudService.getServiceState().name());
                            cloudServiceImpl.put("onlinePlayers", cloudService.getOnlinePlayers());

                            JSONObject groupServiceImpl = new JSONObject();
                            groupServiceImpl.put("name", cloudService.getGroupService().getName());
                            groupServiceImpl.put("maxServer", cloudService.getGroupService().getMaxServers());
                            groupServiceImpl.put("minServer", cloudService.getGroupService().getMinServers());
                            groupServiceImpl.put("maxMemory", cloudService.getGroupService().getMaxMemory());
                            groupServiceImpl.put("percentage", cloudService.getGroupService().getPercentageToStartNewService());
                            groupServiceImpl.put("maxPlayers", cloudService.getGroupService().getMaxPlayers());
                            groupServiceImpl.put("maintenance", cloudService.getGroupService().isMaintenance());
                            groupServiceImpl.put("groupVersion", cloudService.getGroupService().getGroupVersion().getDisplay());
                            groupServiceImpl.put("groupType", cloudService.getGroupService().getGroupType().getDisplay());

                            StringRequest groupServiceImplRequest = new StringRequest();
                            groupServiceImplRequest.value = ConnectionProcess.CLOUDGROUP_SEND_UNIT + ";" + groupServiceImpl.toString();

                            StringRequest cloudServiceImplRequest = new StringRequest();
                            cloudServiceImplRequest.value = ConnectionProcess.CLOUDSERVICE_SEND_UNIT + ";" + cloudServiceImpl.toString();

                            CloudBootstrap.getBootstrap().getLoggerProvider().info("Cloud service §a" + cloudService.getServiceIdName() + " §ris connected.");

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("serviceName", cloudService.getName());
                            jsonObject.put("serviceId", cloudService.getServiceId());
                            jsonObject.put("port", cloudService.getPort());
                            jsonObject.put("groupType", cloudService.getGroupType().getDisplay());

                            for(ICloudService iCloudService : CloudBase.getBase().getCloudServiceManager().getCloudServices()){
                                if(iCloudService.getGroupType().equals(GroupType.PROXY)){
                                    StringRequest jsonRequest = new StringRequest();
                                    jsonRequest.value = ConnectionProcess.CLOUDSERVICE_REGISTERED + ";" + jsonObject.toString();
                                    iCloudService.getConnection().sendTCP(jsonRequest);
                                }
                                if(iCloudService.getConnection() != null){
                                    iCloudService.getConnection().sendTCP(cloudServiceImplRequest);
                                    iCloudService.getConnection().sendTCP(groupServiceImplRequest);
                                }
                            }

                        }

                        if(connectionProcess == ConnectionProcess.CLOUDSERVICE_UNREGISTERED){

                            JSONObject jsonObject = new JSONObject(args[1].replace("[", "").replace("]", ""));
                            String serviceName = jsonObject.getString("serviceName");
                            int serviceId = jsonObject.getInt("serviceId");

                            ICloudService cloudService = CloudBase.getBase().getCloudServiceManager().getCloudServices().stream().filter(service -> service.getServiceIdName().equalsIgnoreCase(serviceName + "-" + serviceId)).findAny().orElse(null);

                            if(cloudService == null){
                                return;
                            }

                            cloudService.stop();

                        }

                        if(connectionProcess == ConnectionProcess.PROXY_PLAYER_CONNECTED || connectionProcess == ConnectionProcess.PROXY_PLAYER_DISCONNECTED){
                            CloudBootstrap.getBootstrap().getLoggerProvider().info(args[2]);
                        }

                        if (connectionProcess == ConnectionProcess.PROXY_PLAYER_SEND_QUERY_TO_CONNECT_SERVER) {
                            JSONObject getObject = new JSONObject(args[1]);
                            String userName = getObject.getString("userName");
                            String serviceName = getObject.getString("serviceName");

                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("userName", userName);
                            jsonObject.put("serviceName", serviceName);

                            StringRequest sendRequest = new StringRequest();
                            sendRequest.value = ConnectionProcess.PROXY_PLAYER_CONNECT_TO_SERVICE + ";" + jsonObject.toString();

                            server.sendToAllTCP(sendRequest);

                        }

                        //TODO: send something

                    }

                }

            });

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public StringResponse getStringResponse(String message){
        StringResponse stringResponse = new StringResponse();
        stringResponse.value = message;
        return stringResponse;
    }

    public void sendUnitQuery(String message){
        StringResponse stringResponse = new StringResponse();
        stringResponse.value = message;
        server.sendToAllTCP(stringResponse);
    }

    public void disconnect(){
        server.stop();
    }

    public Server getServer() {
        return server;
    }
}
