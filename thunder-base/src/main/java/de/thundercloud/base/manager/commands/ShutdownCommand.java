/*
 * » Created by Niklas Sch. on 11.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.manager.commands;

/*

  » de.thundercloud.base.manager.commands

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 11.04.2021 / 21:25

 */

import de.thundercloud.api.command.Command;
import de.thundercloud.api.command.CommandType;
import de.thundercloud.api.command.ICommandHandler;
import de.thundercloud.api.console.ICommandSender;
import de.thundercloud.api.console.LogType;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.base.CloudBase;
import org.jline.reader.Candidate;

import java.util.List;

@Command(name = "shutdown", type = CommandType.CONSOLE)
public class ShutdownCommand implements ICommandHandler {

    @Override
    public void handle(ICommandSender iCommandSender, String[] args) {

        if(args.length == 2) {
            String serviceName = args[1];

            ICloudService cloudService = CloudBase.getBase().getCloudServiceManager().getCloudServices().stream().filter(service -> service.getServiceIdName().equalsIgnoreCase(serviceName)).findAny().orElse(null);

            if (cloudService == null) {
                iCommandSender.sendMessage(LogType.ERROR, "Cloud service §a" + args[0] + " §rwas not found");
                return;
            }

            cloudService.stop();

            try {
                Thread.sleep(3 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            cloudService.start();

        }

    }

    @Override
    public String[] getUsage() {
        return new String[]{"shutdown » Stops a specific service"};
    }

    @Override
    public List<Candidate> getSuggestions() {
        return null;
    }
}
