package de.thundercloud.base;

/*

  » de.thundercloud.base

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:40

 */

import com.google.common.collect.Lists;
import de.thundercloud.base.manager.player.PlayerAdapter;
import de.thundercloud.launcher.files.FileHandler;
import de.thundercloud.base.manager.config.ConfigAdapter;
import de.thundercloud.base.manager.connection.CloudConnectorServer;
import de.thundercloud.base.manager.connection.query.UnitQuery;
import de.thundercloud.base.manager.database.DatabaseHandler;
import de.thundercloud.base.manager.group.CloudGroupServiceManager;
import de.thundercloud.base.manager.service.CloudServiceManager;
import de.thundercloud.base.manager.template.TemplateManager;
import de.thundercloud.base.wrapper.WrapperManager;
import de.thundercloud.launcher.CloudBootstrap;
import de.thundercloud.base.manager.modules.ModuleHandler;

import java.io.File;
import java.util.List;

public class CloudBase {

    private static CloudBase base;

    private final WrapperManager wrapperManager;
    private final CloudServiceManager cloudServiceManager;
    private final DatabaseHandler databaseHandler;
    private final CloudConnectorServer cloudConnectorServer;
    private final CloudGroupServiceManager cloudGroupServiceManager;
    private final UnitQuery unitQuery;
    private final TemplateManager templateManager;
    private final ConfigAdapter configAdapter;
    private final ModuleHandler moduleHandler;
    private final PlayerAdapter playerAdapter;

    public static void main(String[] args) {
        new CloudBase();
    }

    public CloudBase(){
        base = this;

        new CloudBootstrap();

        CloudBootstrap.getBootstrap().getCommandManager().register("de.thundercloud.base.manager.commands");

        List<String> suggestions = Lists.newArrayList();
        CloudBootstrap.getBootstrap().getCommandManager().getCommandHandlers().keySet().forEach(command -> {
            suggestions.add(command.name());
        });
        CloudBootstrap.getBootstrap().getConsoleManager().getConsoleCompleter().setSuggestions(suggestions);

        if(!getFileHandler().fileExist("storage", "config.json")){
            getFileHandler().createFile("storage", "config.json");
            getFileHandler().copyFileOutOfJar(new File("storage", "config.json"), "/config/config.json");
        }

        this.cloudConnectorServer = new CloudConnectorServer();
        this.unitQuery = new UnitQuery();

        this.configAdapter = new ConfigAdapter();
        this.databaseHandler = new DatabaseHandler();
        this.wrapperManager = new WrapperManager();
        this.templateManager = new TemplateManager();

        this.moduleHandler = new ModuleHandler();
        this.moduleHandler.registerModules();

        this.cloudGroupServiceManager = new CloudGroupServiceManager();
        this.cloudServiceManager = new CloudServiceManager();
        this.cloudServiceManager.addAllServices();
        this.playerAdapter = new PlayerAdapter();

        try {
            Thread.sleep(500L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.cloudServiceManager.startAllServices();

    }

    public PlayerAdapter getPlayerAdapter() {
        return playerAdapter;
    }

    public ModuleHandler getModuleHandler() {
        return moduleHandler;
    }

    public ConfigAdapter getConfigAdapter() {
        return configAdapter;
    }

    public FileHandler getFileHandler() {
        return CloudBootstrap.getBootstrap().getFileHandler();
    }

    public TemplateManager getTemplateManager() {
        return templateManager;
    }

    public CloudGroupServiceManager getCloudGroupServiceManager() {
        return cloudGroupServiceManager;
    }

    public UnitQuery getUnitQuery() {
        return unitQuery;
    }

    public CloudConnectorServer getCloudConnectorServer() {
        return cloudConnectorServer;
    }

    public DatabaseHandler getDatabaseHandler() {
        return databaseHandler;
    }

    public CloudServiceManager getCloudServiceManager() {
        return cloudServiceManager;
    }

    public WrapperManager getWrapperManager() {
        return wrapperManager;
    }

    public static CloudBase getBase() {
        return base;
    }
}
