/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.wrapper.configuration.service;

/*

  » de.thundercloud.base.wrapper.configuration.service

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 20:17

 */

import de.thundercloud.api.service.ICloudService;
import de.thundercloud.base.CloudBase;
import de.thundercloud.launcher.files.FileEditor;
import de.thundercloud.base.manager.group.CloudGroupService;
import de.thundercloud.base.wrapper.configuration.IServiceConfiguration;

import java.io.File;

public class DefaultSpigotConfiguration implements IServiceConfiguration {

    @Override
    public void configure(ICloudService cloudService, File file) {
        File newFile = new File(file, "server.properties");
        if(!newFile.exists()){
            CloudBase.getBase().getFileHandler().copyFileOutOfJar(newFile, "/files/server.properties");
        }

        CloudGroupService cloudGroupService = CloudBase.getBase().getCloudGroupServiceManager().getCloudGroupServiceList().stream().filter(cloudGroup -> cloudGroup.getName().equalsIgnoreCase(cloudGroup.getName())).findAny().orElse(null);

        if(cloudGroupService == null) return;

        FileEditor fileEditor = new FileEditor(newFile);
        fileEditor.replaceLine("server-ip=127.0.0.1", "server-ip=127.0.0.1");
        fileEditor.replaceLine("server-port=25565", "server-port=" + cloudService.getPort());
        fileEditor.replaceLine("max-players=20", "max-players=" + cloudGroupService.getMaxPlayers());
        fileEditor.save();

    }

}
