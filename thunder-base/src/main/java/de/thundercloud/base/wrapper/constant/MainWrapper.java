package de.thundercloud.base.wrapper.constant;

/*

  » de.thundercloud.base.wrapper.wrapper

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:48

 */

import de.thundercloud.api.console.Color;
import de.thundercloud.api.wrapper.IWrapper;
import de.thundercloud.api.wrapper.WrapperStatus;
import de.thundercloud.launcher.CloudBootstrap;

public class MainWrapper implements IWrapper {

    private WrapperStatus wrapperStatus;
    private boolean connected = false;

    public MainWrapper() {
        this.wrapperStatus = WrapperStatus.CONNECTING;
    }

    @Override
    public void register() {
        CloudBootstrap.getBootstrap().getLoggerProvider().info("Wrapper " + Color.GREEN + getName() + Color.RESET + " is connecting...");
    }

    @Override
    public void setStatus(WrapperStatus wrapperStatus) {
        this.wrapperStatus = wrapperStatus;
    }

    @Override
    public String getName() {
        return "MainWrapper";
    }

    @Override
    public int getMemory() {
        return 8192;
    }

    @Override
    public WrapperStatus getStatus() {
        return wrapperStatus;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    @Override
    public boolean isConnected() {
        return connected;
    }
}
