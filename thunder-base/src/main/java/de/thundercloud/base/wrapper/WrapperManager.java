package de.thundercloud.base.wrapper;

/*

  » de.thundercloud.base.wrapper

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:41

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.console.Color;
import de.thundercloud.api.wrapper.IWrapper;
import de.thundercloud.api.wrapper.IWrapperManager;
import de.thundercloud.api.wrapper.WrapperStatus;
import de.thundercloud.base.wrapper.constant.MainWrapper;
import de.thundercloud.launcher.CloudBootstrap;

import java.util.List;

public class WrapperManager implements IWrapperManager {

    private final List<IWrapper> wrappers;

    public WrapperManager() {
        this.wrappers = Lists.newArrayList();

        wrappers.add(new MainWrapper());
        for(IWrapper wrapper : wrappers){
            connect(wrapper);
        }
    }

    @Override
    public void connect(IWrapper iWrapper) {
        for(IWrapper wrapper : getWrappers()){
            CloudBootstrap.getBootstrap().getLoggerProvider().info("Wrapper " + Color.GREEN + wrapper.getName() + Color.RESET + " is connected!");
            wrapper.setStatus(WrapperStatus.CONNECTED);
            wrapper.setConnected(true);
        }
    }

    @Override
    public void disconnect(IWrapper iWrapper) {
        for(IWrapper wrapper : getWrappers()){
            CloudBootstrap.getBootstrap().getLoggerProvider().info("Wrapper " + Color.GREEN + wrapper.getName() + Color.RESET + " is disconnecting!");
            wrapper.setStatus(WrapperStatus.DISCONNECTED);
            wrapper.setConnected(false);
        }
    }

    @Override
    public List<IWrapper> getWrappers() {
        return wrappers;
    }

}
