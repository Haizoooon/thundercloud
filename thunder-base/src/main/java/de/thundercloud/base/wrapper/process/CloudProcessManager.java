package de.thundercloud.base.wrapper.process;

/*

  » de.thundercloud.base.wrapper.process

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 31.03.2021 / 15:53

 */

import com.google.common.collect.Lists;
import de.thundercloud.api.connection.ConnectionProcess;
import de.thundercloud.api.group.types.GroupType;
import de.thundercloud.api.group.types.GroupVersion;
import de.thundercloud.api.request.StringRequest;
import de.thundercloud.api.service.ICloudService;
import de.thundercloud.api.service.process.ICloudServiceProcessManager;
import de.thundercloud.api.service.state.ServiceState;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.manager.group.CloudGroupService;
import de.thundercloud.base.manager.service.CloudService;
import de.thundercloud.base.wrapper.configuration.service.DefaultBungeeConfiguration;
import de.thundercloud.base.wrapper.configuration.service.DefaultSpigotConfiguration;
import de.thundercloud.launcher.CloudBootstrap;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class CloudProcessManager implements ICloudServiceProcessManager {

    private Process process;
    private Thread thread;

    private final ICloudService cloudService;

    public CloudProcessManager(ICloudService cloudService) {
        this.cloudService = cloudService;
    }

    public void start(){
        try {

            cloudService.setServiceState(ServiceState.STARTING);

            //TODO: set configuration for service

            if (cloudService.getGroupVersion().equals(GroupVersion.BUNGEECORD) || cloudService.getGroupVersion().equals(GroupVersion.WATERFALL)) {
                DefaultBungeeConfiguration configuration = new DefaultBungeeConfiguration();
                configuration.configure(cloudService, new File("temp/" + cloudService.getServiceIdName()));
            } else {
                DefaultSpigotConfiguration configuration = new DefaultSpigotConfiguration();
                configuration.configure(cloudService, new File("temp/" + cloudService.getServiceIdName()));
            }

            this.process = createProcessBuilder().directory(new File("temp/" + cloudService.getServiceIdName())).start();

            this.thread = new Thread(() -> {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.process.getInputStream()));

                while(this.process.isAlive()){
                    try {

                        String string = (bufferedReader.readLine() == null ? " " : bufferedReader.readLine());

                        if(string.isEmpty()){
                            return;
                        }

                        if(!string.equals(" ") && !string.equals(">") && !string.equals(" >")){
                            this.cloudService.getCachedScreenMessages().add(string);
                        }

                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }
                }

                try {
                    bufferedReader.close();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }

                stop();

            });

            this.thread.start();

        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public void stop() {
        deleteTempFiles();
        this.cloudService.setAuthenticated(false);
        this.cloudService.setOnlinePlayers(0);
        this.cloudService.setServiceState(ServiceState.CLOSED);
        this.cloudService.getCachedScreenMessages().clear();

        for(ICloudService iCloudService : CloudBase.getBase().getCloudServiceManager().getCloudServices()){
            if (iCloudService.getGroupType().equals(GroupType.PROXY)) {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("serviceName", cloudService.getName());
                jsonObject.put("serviceId", cloudService.getServiceId());

                StringRequest stringRequest = new StringRequest();
                stringRequest.value = ConnectionProcess.CLOUDSERVICE_UNREGISTERED + ";" + jsonObject.toString();

                iCloudService.getConnection().sendTCP(stringRequest);
            }
        }
    }

    public void executeCommand(String command){
        String cmd = command + "\n";
        try {
            if(process != null && process.getOutputStream() != null){
                process.getOutputStream().write(cmd.getBytes());
                process.getOutputStream().flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteTempFiles(){
        while (CloudBase.getBase().getFileHandler().folderExist("temp/" + this.cloudService.getServiceIdName())) {

            CloudBase.getBase().getFileHandler().deleteFiles(new File("temp/" + this.cloudService.getServiceIdName()));

            break;
        }
    }

    public List<String> getStartArguments(){
        List<String> arguments = Lists.newArrayList();
        CloudGroupService cloudGroupService = CloudBase.getBase().getCloudGroupServiceManager().getCloudGroupServiceList().stream().filter(groupService -> groupService.getName().equals(cloudService.getName())).findAny().orElse(null);

        assert cloudGroupService != null;

        arguments.add("java");
        arguments.add("-Dcom.mojang.eula.agree=true");
        arguments.add("-Djline.terminal=jline.UnsupportedTerminal");
        arguments.add("-Xms" + cloudGroupService.getMaxMemory() + "m");
        arguments.add("-Xmx" + cloudGroupService.getMaxMemory() + "m");
        arguments.add("-jar");
        arguments.add(cloudService.getGroupVersion().getDisplay() + ".jar");

        return arguments;
    }

    public ProcessBuilder createProcessBuilder(){
        return new ProcessBuilder(getStartArguments());
    }

    @Override
    public ICloudService getCloudService() {
        return cloudService;
    }
}
