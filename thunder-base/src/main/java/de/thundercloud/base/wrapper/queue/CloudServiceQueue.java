/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.wrapper.queue;

/*

  » de.thundercloud.base.wrapper.queue

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 21:06

 */

import de.thundercloud.api.service.process.ICloudServiceProcessManager;
import de.thundercloud.api.service.state.ServiceState;
import de.thundercloud.base.CloudBase;
import de.thundercloud.base.wrapper.process.CloudProcessManager;

import java.util.concurrent.LinkedBlockingQueue;

public class CloudServiceQueue {

    private final LinkedBlockingQueue<ICloudServiceProcessManager> cloudServiceProcessManagers;

    public CloudServiceQueue() {
        this.cloudServiceProcessManagers = new LinkedBlockingQueue<>();

        CloudBase.getBase().getCloudServiceManager().getCloudServices().forEach(cloudService -> {
            cloudServiceProcessManagers.add(new CloudProcessManager(cloudService));
        });

        if(cloudServiceProcessManagers.isEmpty()){
            return;
        }

        cloudServiceProcessManagers.poll().start();

        do {

            this.cloudServiceProcessManagers.removeIf(cloudService -> cloudService.getCloudService().getServiceState() == ServiceState.CLOSED);

            boolean canStartService = !cloudServiceProcessManagers.isEmpty();
            if (canStartService) {
                ICloudServiceProcessManager cloudServiceProcessManager = cloudServiceProcessManagers.poll();


            }

        } while (true);

    }

    public LinkedBlockingQueue<ICloudServiceProcessManager> getCloudServiceProcessManagers() {
        return cloudServiceProcessManagers;
    }
}
