/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.wrapper.configuration.service;

/*

  » de.thundercloud.base.wrapper.configuration

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 20:11

 */

import de.thundercloud.api.service.ICloudService;
import de.thundercloud.base.CloudBase;
import de.thundercloud.launcher.files.FileEditor;
import de.thundercloud.base.manager.group.CloudGroupService;
import de.thundercloud.base.wrapper.configuration.IServiceConfiguration;

import java.io.File;

public class DefaultBungeeConfiguration implements IServiceConfiguration {

    @Override
    public void configure(ICloudService cloudService, File file) {
        File newFile = new File(file, "config.yml");
        if(!newFile.exists()){
            CloudBase.getBase().getFileHandler().copyFileOutOfJar(newFile, "/files/config.yml");
        }
        if(!new File("temp/" + cloudService.getServiceIdName(), "server-icon.png").exists()){
            CloudBase.getBase().getFileHandler().copyFileOutOfJar(new File(file, "server-icon.png"), "/files/server-icon.png");
        }
        CloudGroupService cloudGroupService = CloudBase.getBase().getCloudGroupServiceManager().getCloudGroupServiceList().stream().filter(cloudGroup -> cloudGroup.getName().equalsIgnoreCase(cloudGroup.getName())).findAny().orElse(null);

        if(cloudGroupService == null){
            return;
        }

        FileEditor fileEditor = new FileEditor(newFile);
        fileEditor.replaceLine("  host: 0.0.0.0:25577", "  host: 0.0.0.0:25565");
        fileEditor.replaceLine("  max_players: 1", "  max_players: " +  cloudGroupService.getMaxPlayers());
        fileEditor.save();

    }

}
