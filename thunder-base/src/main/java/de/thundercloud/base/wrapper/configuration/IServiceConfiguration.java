/*
 * » Created by Niklas Sch. on 7.4.2021.
 * » Class by VorGecodet.
 * » This Class/Source cannot be modified without permission.
 *
 * » Discord: VorGecodet#4033
 *
 */

package de.thundercloud.base.wrapper.configuration;

/*

  » de.thundercloud.base.wrapper.configuration

  » Methode/Class coded by Haizoooon#6495
  » This Class/Source cannot be modified without permission.
  » Please refrain from recoding
  » Questions may be asked in Discord

  » Package coded at: 07.04.2021 / 20:11

 */

import de.thundercloud.api.service.ICloudService;

import java.io.File;

public interface IServiceConfiguration {

    void configure(ICloudService cloudService, File file);

}
